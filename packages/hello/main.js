'use strict';

module.exports = {
  load () {
    // execute when package loaded
    // let fs = require('fs');
    // let path = require('path');
    // // automatically create a folder after package loaded
    // fs.mkdirSync(Path.join(Editor.projectPath, 'myNewFolder'));
    // Editor.success('New folder created!');
  },

  unload () {
    // execute when package unloaded
  },

  // register your ipc messages here
  messages: {
    'open' () {
      // open entry panel registered in package.json
      Editor.Panel.open('hello');
    },
    'say-hello' () {
      Editor.log('Hello World!');
      // send ipc message to panel
      Editor.Ipc.sendToPanel('hello', 'hello:hello');
    },
    'clicked' () {
      Editor.log('Button clicked 2!');
      Editor.Scene.callSceneScript('hello', 'get-canvas-children', function (err, length) {
        Console.log ('get-canvas-children callback: length ' + length);
      });
    },
    'scene:saved' () {
      Editor.log('scene savedddd !');
    }
  },
};