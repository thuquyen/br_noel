
z25.png
size: 64,64
format: RGBA8888
filter: Linear,Linear
repeat: none
1111
  rotate: true
  xy: 34, 2
  size: 9, 6
  orig: 9, 6
  offset: 0, 0
  index: -1
Group 3 copy 2
  rotate: false
  xy: 14, 2
  size: 9, 7
  orig: 9, 7
  offset: 0, 0
  index: -1
Group 4 copy
  rotate: true
  xy: 2, 11
  size: 14, 30
  orig: 14, 30
  offset: 0, 0
  index: -1
Group 5 copy
  rotate: false
  xy: 25, 2
  size: 5, 7
  orig: 5, 7
  offset: 0, 0
  index: -1
Layer 74
  rotate: false
  xy: 34, 25
  size: 20, 6
  orig: 20, 6
  offset: 0, 0
  index: -1
Layer 76
  rotate: false
  xy: 31, 33
  size: 28, 29
  orig: 28, 29
  offset: 0, 0
  index: -1
Layer 77 copy
  rotate: true
  xy: 34, 13
  size: 10, 13
  orig: 10, 13
  offset: 0, 0
  index: -1
Layer 82 copy
  rotate: false
  xy: 2, 2
  size: 10, 7
  orig: 10, 7
  offset: 0, 0
  index: -1
Layer 83 copy
  rotate: true
  xy: 56, 21
  size: 10, 6
  orig: 10, 6
  offset: 0, 0
  index: -1
Layer 85 copy
  rotate: true
  xy: 42, 3
  size: 8, 11
  orig: 8, 11
  offset: 0, 0
  index: -1
Layer 86 copy
  rotate: true
  xy: 49, 13
  size: 6, 10
  orig: 6, 10
  offset: 0, 0
  index: -1
Layer 89 copy
  rotate: false
  xy: 2, 27
  size: 27, 35
  orig: 27, 35
  offset: 0, 0
  index: -1
