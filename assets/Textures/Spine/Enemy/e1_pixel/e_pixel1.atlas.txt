
e_pixel1.png
size: 72,72
format: RGBA8888
filter: Linear,Linear
repeat: none
beard
  rotate: false
  xy: 56, 57
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 2
  size: 12, 48
  orig: 12, 48
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 2, 16
  size: 52, 53
  orig: 52, 53
  offset: 0, 0
  index: -1
