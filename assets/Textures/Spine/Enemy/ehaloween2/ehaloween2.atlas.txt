
ehaloween2.png
size: 116,116
format: RGBA8888
filter: Linear,Linear
repeat: none
Group 16 copy 2
  rotate: false
  xy: 2, 25
  size: 54, 87
  orig: 54, 87
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 82, 44
  size: 25, 19
  orig: 25, 19
  offset: 0, 0
  index: -1
ear
  rotate: false
  xy: 37, 3
  size: 12, 20
  orig: 12, 20
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 58, 39
  size: 22, 24
  orig: 22, 24
  offset: 0, 0
  index: -1
stomatch
  rotate: true
  xy: 2, 2
  size: 21, 33
  orig: 21, 33
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 58, 65
  size: 48, 47
  orig: 48, 47
  offset: 0, 0
  index: -1
