
ship16.png
size: 208,208
format: RGBA8888
filter: Linear,Linear
repeat: none
body flare
  rotate: true
  xy: 116, 176
  size: 28, 90
  orig: 28, 90
  offset: 0, 0
  index: -1
body1
  rotate: false
  xy: 116, 132
  size: 54, 42
  orig: 54, 42
  offset: 0, 0
  index: -1
body2
  rotate: true
  xy: 165, 100
  size: 26, 35
  orig: 26, 35
  offset: 0, 0
  index: -1
body3
  rotate: false
  xy: 64, 103
  size: 50, 101
  orig: 50, 101
  offset: 0, 0
  index: -1
body5
  rotate: false
  xy: 2, 35
  size: 37, 62
  orig: 37, 62
  offset: 0, 0
  index: -1
body6
  rotate: true
  xy: 116, 103
  size: 27, 47
  orig: 27, 47
  offset: 0, 0
  index: -1
flare wing body
  rotate: true
  xy: 2, 7
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
flareBody4
  rotate: false
  xy: 70, 58
  size: 37, 43
  orig: 37, 43
  offset: 0, 0
  index: -1
flareNeck
  rotate: false
  xy: 172, 128
  size: 27, 46
  orig: 27, 46
  offset: 0, 0
  index: -1
flarebody3
  rotate: false
  xy: 109, 65
  size: 38, 36
  orig: 38, 36
  offset: 0, 0
  index: -1
neck1
  rotate: true
  xy: 165, 74
  size: 24, 38
  orig: 24, 38
  offset: 0, 0
  index: -1
neck2
  rotate: false
  xy: 41, 51
  size: 27, 46
  orig: 27, 46
  offset: 0, 0
  index: -1
neck3
  rotate: true
  xy: 41, 29
  size: 20, 27
  orig: 20, 27
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 2, 99
  size: 60, 105
  orig: 60, 105
  offset: 0, 0
  index: -1
wing detail 1
  rotate: true
  xy: 109, 37
  size: 9, 19
  orig: 9, 19
  offset: 0, 0
  index: -1
wing detail2
  rotate: false
  xy: 98, 34
  size: 9, 22
  orig: 9, 22
  offset: 0, 0
  index: -1
wing detail3
  rotate: false
  xy: 149, 69
  size: 14, 32
  orig: 14, 32
  offset: 0, 0
  index: -1
wing detail4
  rotate: true
  xy: 109, 48
  size: 15, 42
  orig: 15, 42
  offset: 0, 0
  index: -1
wing light flare
  rotate: true
  xy: 40, 6
  size: 21, 23
  orig: 21, 23
  offset: 0, 0
  index: -1
wingwing
  rotate: false
  xy: 70, 32
  size: 26, 24
  orig: 26, 24
  offset: 0, 0
  index: -1
