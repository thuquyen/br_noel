
GalagaEnemy.png
size: 76,76
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 29, 38
  size: 18, 34
  orig: 18, 34
  offset: 0, 0
  index: -1
canhPhai
  rotate: false
  xy: 2, 38
  size: 25, 34
  orig: 25, 34
  offset: 0, 0
  index: -1
canhTrai
  rotate: false
  xy: 2, 2
  size: 25, 34
  orig: 25, 34
  offset: 0, 0
  index: -1
dot
  rotate: false
  xy: 49, 44
  size: 6, 6
  orig: 6, 6
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 29, 9
  size: 27, 18
  orig: 27, 18
  offset: 0, 0
  index: -1
tayTrai
  rotate: false
  xy: 49, 52
  size: 16, 20
  orig: 16, 20
  offset: 0, 0
  index: -1
