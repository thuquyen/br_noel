
Boss Event.png
size: 288,288
format: RGBA8888
filter: Linear,Linear
repeat: none
MUI
  rotate: false
  xy: 74, 2
  size: 42, 25
  orig: 42, 25
  offset: 0, 0
  index: -1
dinh dau
  rotate: false
  xy: 247, 130
  size: 28, 29
  orig: 28, 29
  offset: 0, 0
  index: -1
dong tu mat
  rotate: false
  xy: 118, 11
  size: 17, 16
  orig: 17, 16
  offset: 0, 0
  index: -1
duoi sung
  rotate: true
  xy: 198, 43
  size: 65, 64
  orig: 65, 64
  offset: 0, 0
  index: -1
mat lan
  rotate: false
  xy: 2, 190
  size: 126, 96
  orig: 126, 96
  offset: 0, 0
  index: -1
mi mat duoi
  rotate: false
  xy: 239, 193
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
mi mat tren
  rotate: true
  xy: 191, 110
  size: 69, 54
  orig: 69, 54
  offset: 0, 0
  index: -1
rang duoi
  rotate: false
  xy: 130, 181
  size: 107, 54
  orig: 107, 54
  offset: 0, 0
  index: -1
rang tren
  rotate: false
  xy: 130, 237
  size: 155, 49
  orig: 155, 49
  offset: 0, 0
  index: -1
than duoi sung
  rotate: false
  xy: 2, 4
  size: 70, 59
  orig: 70, 59
  offset: 0, 0
  index: -1
than tren sung
  rotate: false
  xy: 98, 101
  size: 91, 78
  orig: 91, 78
  offset: 0, 0
  index: -1
trong mat
  rotate: false
  xy: 247, 161
  size: 30, 30
  orig: 30, 30
  offset: 0, 0
  index: -1
vien mieng 1
  rotate: false
  xy: 2, 65
  size: 94, 123
  orig: 94, 123
  offset: 0, 0
  index: -1
vien mieng 2
  rotate: true
  xy: 98, 29
  size: 70, 98
  orig: 70, 98
  offset: 0, 0
  index: -1
