
Boss28.png
size: 224,224
format: RGBA8888
filter: Linear,Linear
repeat: none
3
  rotate: false
  xy: 87, 25
  size: 48, 51
  orig: 48, 51
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 87, 5
  size: 32, 18
  orig: 32, 18
  offset: 0, 0
  index: -1
5
  rotate: true
  xy: 196, 139
  size: 31, 18
  orig: 31, 18
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 196, 172
  size: 18, 32
  orig: 18, 32
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 182, 47
  size: 18, 31
  orig: 18, 31
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 180, 96
  size: 41, 41
  orig: 41, 41
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 3
  size: 83, 94
  orig: 83, 94
  offset: 0, 0
  index: -1
gun
  rotate: true
  xy: 180, 80
  size: 14, 31
  orig: 14, 31
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 119, 139
  size: 75, 81
  orig: 75, 81
  offset: 0, 0
  index: -1
horn
  rotate: true
  xy: 196, 206
  size: 14, 26
  orig: 14, 26
  offset: 0, 0
  index: -1
plate wing1
  rotate: false
  xy: 137, 33
  size: 43, 43
  orig: 43, 43
  offset: 0, 0
  index: -1
plate wing2
  rotate: false
  xy: 119, 78
  size: 59, 59
  orig: 59, 59
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 2, 99
  size: 115, 121
  orig: 115, 121
  offset: 0, 0
  index: -1
