
boss13.png
size: 308,308
format: RGBA8888
filter: Linear,Linear
repeat: none
arm
  rotate: false
  xy: 202, 91
  size: 67, 60
  orig: 67, 60
  offset: 0, 0
  index: -1
back
  rotate: false
  xy: 258, 39
  size: 40, 50
  orig: 40, 50
  offset: 0, 0
  index: -1
back_deco
  rotate: false
  xy: 58, 5
  size: 51, 20
  orig: 51, 20
  offset: 0, 0
  index: -1
back_gun
  rotate: true
  xy: 147, 31
  size: 21, 31
  orig: 21, 31
  offset: 0, 0
  index: -1
hand
  rotate: true
  xy: 134, 232
  size: 73, 133
  orig: 73, 133
  offset: 0, 0
  index: -1
hand2
  rotate: true
  xy: 2, 2
  size: 23, 54
  orig: 23, 54
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 194, 31
  size: 58, 62
  orig: 58, 62
  offset: 0, 0
  index: -1
head_gun
  rotate: true
  xy: 106, 70
  size: 13, 17
  orig: 13, 17
  offset: 0, 0
  index: -1
lats
  rotate: true
  xy: 125, 94
  size: 62, 75
  orig: 62, 75
  offset: 0, 0
  index: -1
light
  rotate: false
  xy: 125, 172
  size: 6, 6
  orig: 6, 6
  offset: 0, 0
  index: -1
low_back
  rotate: false
  xy: 2, 85
  size: 121, 93
  orig: 121, 93
  offset: 0, 0
  index: -1
neck
  rotate: true
  xy: 216, 153
  size: 77, 48
  orig: 77, 48
  offset: 0, 0
  index: -1
rocket_light
  rotate: false
  xy: 111, 2
  size: 28, 23
  orig: 28, 23
  offset: 0, 0
  index: -1
stomatch
  rotate: false
  xy: 2, 180
  size: 130, 125
  orig: 130, 125
  offset: 0, 0
  index: -1
stomatch_gun
  rotate: true
  xy: 2, 27
  size: 56, 102
  orig: 56, 102
  offset: 0, 0
  index: -1
stomatch_gun_deco
  rotate: true
  xy: 125, 54
  size: 38, 67
  orig: 38, 67
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 134, 158
  size: 80, 72
  orig: 80, 72
  offset: 0, 0
  index: -1
teeth
  rotate: true
  xy: 106, 27
  size: 25, 39
  orig: 25, 39
  offset: 0, 0
  index: -1
throne
  rotate: false
  xy: 269, 233
  size: 34, 72
  orig: 34, 72
  offset: 0, 0
  index: -1
