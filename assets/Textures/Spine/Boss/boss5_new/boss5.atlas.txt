
boss5.png
size: 244,244
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 85
  size: 214, 155
  orig: 214, 155
  offset: 0, 0
  index: -1
deco
  rotate: true
  xy: 94, 12
  size: 30, 66
  orig: 30, 66
  offset: 0, 0
  index: -1
gun1
  rotate: true
  xy: 94, 44
  size: 39, 92
  orig: 39, 92
  offset: 0, 0
  index: -1
guna
  rotate: false
  xy: 188, 25
  size: 45, 58
  orig: 45, 58
  offset: 0, 0
  index: -1
gunx
  rotate: false
  xy: 218, 196
  size: 13, 44
  orig: 13, 44
  offset: 0, 0
  index: -1
light
  rotate: true
  xy: 2, 2
  size: 81, 90
  orig: 81, 90
  offset: 0, 0
  index: -1
