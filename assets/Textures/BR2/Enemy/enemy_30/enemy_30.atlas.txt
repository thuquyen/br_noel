
enemy_30.png
size: 176,176
format: RGBA8888
filter: Linear,Linear
repeat: none
a1
  rotate: false
  xy: 83, 55
  size: 33, 33
  orig: 33, 33
  offset: 0, 0
  index: -1
as2
  rotate: true
  xy: 93, 92
  size: 31, 40
  orig: 31, 40
  offset: 0, 0
  index: -1
as3
  rotate: false
  xy: 153, 126
  size: 20, 27
  orig: 20, 27
  offset: 0, 0
  index: -1
as4
  rotate: false
  xy: 2, 90
  size: 89, 82
  orig: 89, 82
  offset: 0, 0
  index: -1
c1
  rotate: false
  xy: 118, 64
  size: 14, 26
  orig: 14, 26
  offset: 0, 0
  index: -1
cc
  rotate: true
  xy: 83, 29
  size: 24, 31
  orig: 24, 31
  offset: 0, 0
  index: -1
eys1
  rotate: false
  xy: 153, 155
  size: 21, 17
  orig: 21, 17
  offset: 0, 0
  index: -1
h1
  rotate: true
  xy: 57, 27
  size: 61, 24
  orig: 61, 24
  offset: 0, 0
  index: -1
t1
  rotate: false
  xy: 93, 125
  size: 58, 47
  orig: 58, 47
  offset: 0, 0
  index: -1
w2
  rotate: false
  xy: 2, 13
  size: 53, 75
  orig: 53, 75
  offset: 0, 0
  index: -1
w4
  rotate: false
  xy: 135, 92
  size: 39, 31
  orig: 39, 31
  offset: 0, 0
  index: -1
