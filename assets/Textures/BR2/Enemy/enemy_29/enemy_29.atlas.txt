
enemy_29.png
size: 192,192
format: RGBA8888
filter: Linear,Linear
repeat: none
0
  rotate: false
  xy: 2, 19
  size: 38, 59
  orig: 38, 59
  offset: 0, 0
  index: -1
1
  rotate: false
  xy: 82, 2
  size: 17, 35
  orig: 17, 35
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 82, 39
  size: 30, 51
  orig: 30, 51
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 114, 39
  size: 30, 51
  orig: 30, 51
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 2, 80
  size: 76, 50
  orig: 76, 50
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 101, 17
  size: 22, 20
  orig: 22, 20
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 113, 142
  size: 75, 48
  orig: 75, 48
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 2, 132
  size: 109, 58
  orig: 109, 58
  offset: 0, 0
  index: -1
8
  rotate: true
  xy: 80, 92
  size: 38, 51
  orig: 38, 51
  offset: 0, 0
  index: -1
9
  rotate: true
  xy: 133, 102
  size: 38, 51
  orig: 38, 51
  offset: 0, 0
  index: -1
a1
  rotate: false
  xy: 146, 67
  size: 33, 33
  orig: 33, 33
  offset: 0, 0
  index: -1
x
  rotate: false
  xy: 146, 40
  size: 21, 25
  orig: 21, 25
  offset: 0, 0
  index: -1
y
  rotate: true
  xy: 42, 19
  size: 59, 38
  orig: 59, 38
  offset: 0, 0
  index: -1
