
dan.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: false
  xy: 373, 376
  size: 120, 120
  orig: 120, 120
  offset: 0, 0
  index: -1
Layer 2
  rotate: false
  xy: 251, 366
  size: 120, 130
  orig: 120, 130
  offset: 0, 0
  index: -1
Layer 3
  rotate: false
  xy: 373, 267
  size: 120, 107
  orig: 120, 107
  offset: 0, 0
  index: -1
Layer 4
  rotate: false
  xy: 251, 246
  size: 120, 118
  orig: 120, 118
  offset: 0, 0
  index: -1
Layer 5
  rotate: false
  xy: 2, 2
  size: 245, 245
  orig: 245, 245
  offset: 0, 0
  index: -1
Layer 836 copy 2
  rotate: false
  xy: 2, 249
  size: 247, 247
  orig: 247, 247
  offset: 0, 0
  index: -1
