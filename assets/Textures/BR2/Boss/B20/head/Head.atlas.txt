
Head.png
size: 468,468
format: RGBA8888
filter: Linear,Linear
repeat: none
c12
  rotate: false
  xy: 337, 157
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
c13
  rotate: true
  xy: 281, 216
  size: 55, 96
  orig: 55, 96
  offset: 0, 0
  index: -1
c13xxx
  rotate: false
  xy: 150, 146
  size: 55, 96
  orig: 55, 96
  offset: 0, 0
  index: -1
caico1
  rotate: false
  xy: 182, 350
  size: 98, 116
  orig: 98, 116
  offset: 0, 0
  index: -1
camxx
  rotate: true
  xy: 338, 81
  size: 74, 49
  orig: 74, 49
  offset: 0, 0
  index: -1
chan1
  rotate: true
  xy: 264, 150
  size: 64, 71
  orig: 64, 71
  offset: 0, 0
  index: -1
chan2
  rotate: false
  xy: 272, 77
  size: 64, 71
  orig: 64, 71
  offset: 0, 0
  index: -1
chan3x
  rotate: false
  xy: 389, 70
  size: 40, 85
  orig: 40, 85
  offset: 0, 0
  index: -1
chanwhite1
  rotate: false
  xy: 405, 365
  size: 61, 101
  orig: 61, 101
  offset: 0, 0
  index: -1
chanwhite2
  rotate: true
  xy: 281, 273
  size: 61, 101
  orig: 61, 101
  offset: 0, 0
  index: -1
chanx
  rotate: true
  xy: 397, 310
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
chanx1
  rotate: false
  xy: 207, 146
  size: 55, 96
  orig: 55, 96
  offset: 0, 0
  index: -1
chanx1xxx
  rotate: false
  xy: 156, 48
  size: 55, 96
  orig: 55, 96
  offset: 0, 0
  index: -1
chanxxxx2
  rotate: false
  xy: 379, 220
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
dau1
  rotate: false
  xy: 2, 324
  size: 178, 142
  orig: 178, 142
  offset: 0, 0
  index: -1
dau2
  rotate: false
  xy: 2, 256
  size: 151, 66
  orig: 151, 66
  offset: 0, 0
  index: -1
e1
  rotate: true
  xy: 427, 224
  size: 37, 39
  orig: 37, 39
  offset: 0, 0
  index: -1
e2
  rotate: true
  xy: 392, 172
  size: 46, 40
  orig: 46, 40
  offset: 0, 0
  index: -1
e3
  rotate: false
  xy: 431, 136
  size: 35, 34
  orig: 35, 34
  offset: 0, 0
  index: -1
e4
  rotate: true
  xy: 384, 263
  size: 45, 47
  orig: 45, 47
  offset: 0, 0
  index: -1
e5
  rotate: true
  xy: 156, 2
  size: 44, 48
  orig: 44, 48
  offset: 0, 0
  index: -1
hamrangx
  rotate: false
  xy: 213, 66
  size: 57, 78
  orig: 57, 78
  offset: 0, 0
  index: -1
ln1
  rotate: true
  xy: 2, 27
  size: 104, 97
  orig: 104, 97
  offset: 0, 0
  index: -1
ln2
  rotate: true
  xy: 182, 244
  size: 104, 97
  orig: 104, 97
  offset: 0, 0
  index: -1
longmay1
  rotate: false
  xy: 2, 133
  size: 91, 121
  orig: 91, 121
  offset: 0, 0
  index: -1
longmay2
  rotate: true
  xy: 282, 375
  size: 91, 121
  orig: 91, 121
  offset: 0, 0
  index: -1
longmay3
  rotate: false
  xy: 95, 141
  size: 53, 113
  orig: 53, 113
  offset: 0, 0
  index: -1
longmay4
  rotate: false
  xy: 101, 26
  size: 53, 113
  orig: 53, 113
  offset: 0, 0
  index: -1
matdo
  rotate: false
  xy: 434, 188
  size: 32, 34
  orig: 32, 34
  offset: 0, 0
  index: -1
matxanh
  rotate: true
  xy: 433, 277
  size: 31, 33
  orig: 31, 33
  offset: 0, 0
  index: -1
tai2
  rotate: true
  xy: 282, 336
  size: 37, 113
  orig: 37, 113
  offset: 0, 0
  index: -1
vcz
  rotate: false
  xy: 213, 23
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
