
boss15.png
size: 380,380
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 10
  rotate: false
  xy: 208, 21
  size: 53, 105
  orig: 53, 105
  offset: 0, 0
  index: -1
Layer 6
  rotate: false
  xy: 2, 15
  size: 204, 155
  orig: 204, 155
  offset: 0, 0
  index: -1
Layer 8
  rotate: true
  xy: 203, 281
  size: 95, 175
  orig: 95, 175
  offset: 0, 0
  index: -1
Layer 8_1
  rotate: false
  xy: 324, 76
  size: 48, 50
  orig: 48, 50
  offset: 0, 0
  index: -1
Layer 9
  rotate: false
  xy: 208, 128
  size: 167, 151
  orig: 167, 151
  offset: 0, 0
  index: -1
Layer 98 copy
  rotate: false
  xy: 2, 172
  size: 199, 204
  orig: 199, 204
  offset: 0, 0
  index: -1
Layer 9_1
  rotate: false
  xy: 263, 22
  size: 37, 40
  orig: 37, 40
  offset: 0, 0
  index: -1
l1
  rotate: false
  xy: 263, 64
  size: 59, 62
  orig: 59, 62
  offset: 0, 0
  index: -1
