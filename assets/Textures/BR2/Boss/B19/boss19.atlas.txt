
boss19.png
size: 332,332
format: RGBA8888
filter: Linear,Linear
repeat: none
bodyend1
  rotate: true
  xy: 247, 264
  size: 66, 83
  orig: 66, 83
  offset: 0, 0
  index: -1
bodyend2
  rotate: true
  xy: 168, 3
  size: 17, 30
  orig: 17, 30
  offset: 0, 0
  index: -1
bodyend3
  rotate: false
  xy: 247, 228
  size: 67, 34
  orig: 67, 34
  offset: 0, 0
  index: -1
bodyend4
  rotate: true
  xy: 152, 86
  size: 61, 58
  orig: 61, 58
  offset: 0, 0
  index: -1
brain1
  rotate: true
  xy: 209, 165
  size: 28, 57
  orig: 28, 57
  offset: 0, 0
  index: -1
ear1
  rotate: true
  xy: 212, 195
  size: 31, 102
  orig: 31, 102
  offset: 0, 0
  index: -1
earnx1
  rotate: true
  xy: 268, 150
  size: 19, 44
  orig: 19, 44
  offset: 0, 0
  index: -1
ex
  rotate: true
  xy: 316, 247
  size: 15, 14
  orig: 15, 14
  offset: 0, 0
  index: -1
eye1
  rotate: false
  xy: 76, 86
  size: 74, 61
  orig: 74, 61
  offset: 0, 0
  index: -1
eyed2
  rotate: false
  xy: 212, 106
  size: 50, 57
  orig: 50, 57
  offset: 0, 0
  index: -1
head1
  rotate: false
  xy: 175, 197
  size: 35, 42
  orig: 35, 42
  offset: 0, 0
  index: -1
heart1
  rotate: false
  xy: 126, 3
  size: 40, 81
  orig: 40, 81
  offset: 0, 0
  index: -1
hop1
  rotate: false
  xy: 264, 116
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
ineyd
  rotate: false
  xy: 264, 95
  size: 26, 19
  orig: 26, 19
  offset: 0, 0
  index: -1
ineyds2
  rotate: true
  xy: 212, 228
  size: 11, 14
  orig: 11, 14
  offset: 0, 0
  index: -1
l1
  rotate: false
  xy: 168, 22
  size: 59, 62
  orig: 59, 62
  offset: 0, 0
  index: -1
leg1
  rotate: false
  xy: 173, 161
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
lung1
  rotate: false
  xy: 104, 149
  size: 67, 86
  orig: 67, 86
  offset: 0, 0
  index: -1
mouth1
  rotate: false
  xy: 229, 81
  size: 23, 23
  orig: 23, 23
  offset: 0, 0
  index: -1
mui1
  rotate: true
  xy: 268, 171
  size: 22, 53
  orig: 22, 53
  offset: 0, 0
  index: -1
neck1
  rotate: true
  xy: 2, 2
  size: 51, 80
  orig: 51, 80
  offset: 0, 0
  index: -1
neck2
  rotate: false
  xy: 84, 2
  size: 40, 82
  orig: 40, 82
  offset: 0, 0
  index: -1
shoulder1
  rotate: false
  xy: 175, 241
  size: 70, 89
  orig: 70, 89
  offset: 0, 0
  index: -1
shoulder2
  rotate: false
  xy: 104, 237
  size: 69, 93
  orig: 69, 93
  offset: 0, 0
  index: -1
shoulderx
  rotate: false
  xy: 2, 155
  size: 100, 175
  orig: 100, 175
  offset: 0, 0
  index: -1
skill1
  rotate: false
  xy: 2, 55
  size: 72, 98
  orig: 72, 98
  offset: 0, 0
  index: -1
zzzz1
  rotate: false
  xy: 298, 117
  size: 29, 31
  orig: 29, 31
  offset: 0, 0
  index: -1
