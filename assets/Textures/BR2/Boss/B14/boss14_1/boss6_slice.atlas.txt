
boss6_slice.png
size: 476,476
format: RGBA8888
filter: Linear,Linear
repeat: none
canh3
  rotate: false
  xy: 326, 119
  size: 74, 77
  orig: 74, 77
  offset: 0, 0
  index: -1
canh4
  rotate: true
  xy: 420, 211
  size: 88, 49
  orig: 88, 49
  offset: 0, 0
  index: -1
canh5
  rotate: false
  xy: 244, 8
  size: 69, 52
  orig: 69, 52
  offset: 0, 0
  index: -1
canh6
  rotate: false
  xy: 374, 7
  size: 52, 48
  orig: 52, 48
  offset: 0, 0
  index: -1
chan1
  rotate: false
  xy: 218, 150
  size: 101, 122
  orig: 101, 122
  offset: 0, 0
  index: -1
chan2
  rotate: false
  xy: 164, 19
  size: 27, 20
  orig: 27, 20
  offset: 0, 0
  index: -1
chan3
  rotate: false
  xy: 193, 16
  size: 23, 23
  orig: 23, 23
  offset: 0, 0
  index: -1
dau1
  rotate: true
  xy: 48, 6
  size: 28, 68
  orig: 28, 68
  offset: 0, 0
  index: -1
dau2
  rotate: false
  xy: 157, 129
  size: 59, 140
  orig: 59, 140
  offset: 0, 0
  index: -1
daux
  rotate: false
  xy: 402, 134
  size: 33, 62
  orig: 33, 62
  offset: 0, 0
  index: -1
duoi1
  rotate: false
  xy: 321, 198
  size: 97, 101
  orig: 97, 101
  offset: 0, 0
  index: -1
l1
  rotate: false
  xy: 411, 70
  size: 59, 62
  orig: 59, 62
  offset: 0, 0
  index: -1
madau
  rotate: true
  xy: 315, 8
  size: 47, 57
  orig: 47, 57
  offset: 0, 0
  index: -1
mat
  rotate: true
  xy: 428, 8
  size: 28, 40
  orig: 28, 40
  offset: 0, 0
  index: -1
mom
  rotate: true
  xy: 428, 38
  size: 30, 46
  orig: 30, 46
  offset: 0, 0
  index: -1
nhannhan
  rotate: true
  xy: 118, 17
  size: 17, 44
  orig: 17, 44
  offset: 0, 0
  index: -1
sung1
  rotate: true
  xy: 2, 2
  size: 32, 44
  orig: 32, 44
  offset: 0, 0
  index: -1
than1
  rotate: true
  xy: 313, 301
  size: 100, 156
  orig: 100, 156
  offset: 0, 0
  index: -1
thanduoi1
  rotate: true
  xy: 157, 41
  size: 86, 85
  orig: 86, 85
  offset: 0, 0
  index: -1
thanduoi2
  rotate: true
  xy: 313, 403
  size: 71, 161
  orig: 71, 161
  offset: 0, 0
  index: -1
thanduoi3
  rotate: true
  xy: 326, 57
  size: 60, 83
  orig: 60, 83
  offset: 0, 0
  index: -1
thanduoi4
  rotate: false
  xy: 2, 36
  size: 153, 233
  orig: 153, 233
  offset: 0, 0
  index: -1
vai1
  rotate: true
  xy: 244, 62
  size: 86, 80
  orig: 86, 80
  offset: 0, 0
  index: -1
xcanh1
  rotate: false
  xy: 2, 271
  size: 211, 203
  orig: 211, 203
  offset: 0, 0
  index: -1
xcanh2
  rotate: false
  xy: 215, 274
  size: 96, 200
  orig: 96, 200
  offset: 0, 0
  index: -1
