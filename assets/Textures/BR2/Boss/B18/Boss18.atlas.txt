
Boss18.png
size: 336,336
format: RGBA8888
filter: Linear,Linear
repeat: none
black1
  rotate: false
  xy: 57, 45
  size: 26, 127
  orig: 26, 127
  offset: 0, 0
  index: -1
black2
  rotate: false
  xy: 85, 72
  size: 54, 106
  orig: 54, 106
  offset: 0, 0
  index: -1
black3
  rotate: true
  xy: 249, 272
  size: 62, 83
  orig: 62, 83
  offset: 0, 0
  index: -1
black4
  rotate: true
  xy: 168, 55
  size: 50, 65
  orig: 50, 65
  offset: 0, 0
  index: -1
black5
  rotate: false
  xy: 148, 18
  size: 42, 35
  orig: 42, 35
  offset: 0, 0
  index: -1
black6
  rotate: false
  xy: 81, 180
  size: 79, 115
  orig: 79, 115
  offset: 0, 0
  index: -1
black7
  rotate: false
  xy: 192, 10
  size: 25, 43
  orig: 25, 43
  offset: 0, 0
  index: -1
black8
  rotate: true
  xy: 87, 3
  size: 37, 59
  orig: 37, 59
  offset: 0, 0
  index: -1
black_eyes
  rotate: false
  xy: 312, 215
  size: 20, 15
  orig: 20, 15
  offset: 0, 0
  index: -1
black_t_1
  rotate: false
  xy: 2, 297
  size: 245, 37
  orig: 245, 37
  offset: 0, 0
  index: -1
blackx
  rotate: false
  xy: 228, 125
  size: 48, 76
  orig: 48, 76
  offset: 0, 0
  index: -1
gold1
  rotate: false
  xy: 219, 31
  size: 17, 17
  orig: 17, 17
  offset: 0, 0
  index: -1
gold10
  rotate: false
  xy: 278, 137
  size: 44, 75
  orig: 44, 75
  offset: 0, 0
  index: -1
gold11
  rotate: false
  xy: 235, 50
  size: 35, 73
  orig: 35, 73
  offset: 0, 0
  index: -1
gold12
  rotate: true
  xy: 85, 42
  size: 28, 52
  orig: 28, 52
  offset: 0, 0
  index: -1
gold2
  rotate: false
  xy: 141, 59
  size: 25, 119
  orig: 25, 119
  offset: 0, 0
  index: -1
gold3
  rotate: false
  xy: 168, 107
  size: 58, 94
  orig: 58, 94
  offset: 0, 0
  index: -1
gold4
  rotate: true
  xy: 239, 214
  size: 56, 71
  orig: 56, 71
  offset: 0, 0
  index: -1
gold5 copy
  rotate: true
  xy: 272, 2
  size: 60, 61
  orig: 60, 61
  offset: 0, 0
  index: -1
gold6
  rotate: false
  xy: 2, 174
  size: 77, 121
  orig: 77, 121
  offset: 0, 0
  index: -1
gold7
  rotate: true
  xy: 46, 2
  size: 38, 39
  orig: 38, 39
  offset: 0, 0
  index: -1
gold8
  rotate: false
  xy: 2, 12
  size: 42, 39
  orig: 42, 39
  offset: 0, 0
  index: -1
gold9
  rotate: false
  xy: 312, 232
  size: 21, 38
  orig: 21, 38
  offset: 0, 0
  index: -1
l1
  rotate: true
  xy: 272, 64
  size: 59, 62
  orig: 59, 62
  offset: 0, 0
  index: -1
ten1
  rotate: false
  xy: 162, 203
  size: 75, 92
  orig: 75, 92
  offset: 0, 0
  index: -1
ten2
  rotate: false
  xy: 2, 53
  size: 53, 119
  orig: 53, 119
  offset: 0, 0
  index: -1
