import BaseEnemyInfo from "./BaseEnemyInfo";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class StraightWave extends cc.Component {

    points: cc.Vec2[] = [];

    @property(Number)
    speed: number = 300;

    @property(Number)
    yTop: number = 480;

    @property(BaseEnemyInfo)
    enemyInfo: BaseEnemyInfo = null;

    start () {
        this.points = [];
        for(var i=0; i<this.node.children.length; i++) {
            this.points.push(this.node.children[i].getPosition());
        }
        this.startAttack();
    }

    startAttack(){
        for(var i=0; i<this.points.length; i++) {
            this.scheduleEnemy(i);
        }
    }

    scheduleEnemy(i){
        this.scheduleOnce(()=>{
            var e = this.enemyInfo.spawnEnemy();
            e.x = this.points[i].x;
            e.y = this.yTop;
            this.moveEnemy(e);
        }, (this.points[i].y-this.yTop)/this.speed);
    }

    moveEnemy(node: cc.Node){
        node.runAction(cc.moveBy(1000/this.speed, 0, -1000));
    }

}