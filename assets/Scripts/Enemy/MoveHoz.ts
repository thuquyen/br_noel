import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    enemy: cc.Node[] = []
    isRun: boolean = false;
    posX: number = 0;
    posY: number = 0;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }
    moveHoz() {
        // this.scheduleOnce(() => {
        for (let i = 0; i < this.enemy.length; i++) {
            if (this.enemy[i].active) {
                this.schedule(() => {
                    this.enemy[i].runAction(cc.sequence(
                        cc.moveBy(1, this.posX, this.posY),
                        cc.moveBy(1, this.posX, this.posY)
                    ))
                }, 0.5)
            }
        }
        // }, 0.9)
    }
    update(dt) {
        if (G.isZigzag && !this.isRun) {
            this.moveHoz();
            G.isZigzag = false;
            this.isRun = true;
        }
        this.posX = (Math.random() * 2 - 1) * 50;
        this.posY = (Math.random() * 2 - 1) * 50;
        if (this.posX > this.node.parent.x || this.posX < - this.node.parent.x) {
            this.posX = this.node.parent.x;
        }
        if (this.posY > this.node.parent.y || this.posY < - this.node.parent.y) {
            this.posY = this.node.parent.y
        }
    }
}
