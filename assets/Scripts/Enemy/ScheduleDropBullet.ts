import ScheduleSkill from "./ScheduleSkill";
import { Helper } from "../Helper";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu('Skills/ScheduleDropBullet')
export default class ScheduleDropBullet extends ScheduleSkill {

    @property(Number)
    speed: number = 300;

    @property(Number)
    deltaSpeed: number = 100;

    attack(){
        var speed = this.speed + Math.random()*this.deltaSpeed;
        var eBullet = Helper.poolManager.eBullet.createObject(this.node.parent);
        eBullet.x = this.node.x;
        eBullet.y = this.node.y;
        eBullet.runAction(cc.sequence(
            cc.moveBy(1500/speed, 0, -1500),
            cc.callFunc(()=>{
                Helper.poolManager.eBullet.killObject(eBullet);
            })
        ));
        this.scheduleOnce(()=>{
            eBullet.zIndex = 2;
        }, 100);
    }

}