import ESkill from "./ESkill";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass('ConfigSpawnBullet1')
export class ConfigSpawnBullet1 {

    @property(Number)
    delay: number = 3.5;

    @property(Number)
    interval: number = 3;

    @property(Number)
    deltaInterval: number = 1;

    @property(Number)
    speed: number = 300;

    @property(Number)
    deltaSpeed: number = 100;

    @property(String)
    pattern: String = '9';

}

@ccclass
export default class ESkillSpawnBullet extends ESkill {

    @property(cc.Prefab)
    bullet: cc.Prefab[] = [];

    @property()
    time: number = 0;

    @property()
    iBullet: number = 0;

    private skillBoss: cc.Prefab = null;

    @property(ConfigSpawnBullet1)
    config: ConfigSpawnBullet1 = new ConfigSpawnBullet1();

    private i = 0;

    strAnimCurrent: string = '';

    start() {
        this.getComponent(sp.Skeleton).setMix('attack', 'idle', 0.25);
        this.getComponent(sp.Skeleton).setMix('idle', 'attack', 0.1);
        this.changeAnimation(1, 'idle', true);

        this.scheduleOnce(() => {
            this.schedule(() => {
                let x = cc.Canvas.instance.node.width / 2 * (Math.random() - 0.5);
                this.node.runAction(cc.sequence(cc.moveTo(1, cc.v2(x, this.node.y)), cc.callFunc(() => {
                    this.bulletBoss();
                })));
            }, this.config.interval, cc.macro.REPEAT_FOREVER, this.config.delay);
        }, this.config.delay);
    }

    hitBullet() {
        this.node.runAction(cc.sequence(cc.callFunc(() => {
            this.node.color = cc.Color.RED;
        }), cc.callFunc(() => {
            this.scheduleOnce(() => this.node.color = cc.Color.WHITE, 0.1);
        })));
    }

    skill() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'attack', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'idle', true);
            }, 1);
        } else if (this.iBullet === 0) {
            this.changeAnimation(1, 'idle', true);
        }
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }

    public bulletBoss() {
        if (G.e == false) {
            this.iBullet++;
            if (this.iBullet == this.bullet.length) {
                this.iBullet = 0;
            }
            this.skill();
            this.skillBoss = this.bullet[this.iBullet];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.x = this.node.x;
            newBullet.y = this.node.y;
            newBullet.zIndex = 1;
        } else {
            this.changeAnimation(1, 'idle', true);
            return;
        }

    }

}