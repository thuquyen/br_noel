import BasePool from "../common/BasePool";
import { Helper } from "../Helper";
import IEnemy from "./IEnemy";
import G from "../Global";

const { ccclass, property } = cc._decorator;

export enum EnemyEvent {
    INIT,
    APPEARED,
    HIT_BULLET,
    DIE
};

@ccclass
export default class BaseEnemy extends IEnemy {

    _isDie: boolean = false;

    public score: number = 0;
    // @property(cc.Prefab)
    // explosionPrefab: cc.Prefab = null;

    @property(BasePool)
    explosionPool: BasePool = null;

    @property(BasePool)
    hitExplosionPool: BasePool = null;

    centrePos: cc.Vec2 = null;

    @property(cc.Vec2)
    adjustExplosion: cc.Vec2 = null;

    @property(Number)
    scaleExplosion: number = 1.1;

    @property(Number)
    speedExplosion: number = 2;

    @property(Number)
    public hp: number = 1;

    @property(Number)
    dmg: number = 0;

    @property()
    check: number = 0;

    checkExplosion = 0;
    checkSoundBoss: number = 0;

    // @property(cc.Node)
    // bar: cc.Node = null;
    hpTotal: number = 0;
    widthBar: number = 0;

    setOnDie: Function = null;

    @property(cc.Prefab)
    coinPrefab: cc.Prefab = null;
    @property(cc.Node)
    coinNode: cc.Node = null;
    coinList: cc.Node[] = [];

    start() {
        this.hpTotal = this.hp;
        // if (this.bar)
        //     this.widthBar = this.bar.children[0].scaleX;
        var centrePos = this.node.getChildByName('centrePos');
        if (centrePos != null) {
            this.centrePos = centrePos.getPosition();
        } else {
            if (this.adjustExplosion != null) {
                this.centrePos = this.adjustExplosion;
            } else {
                this.centrePos = new cc.Vec2(0, this.node.height / 2);
            }
        }
        if (this.explosionPool == null)
            this.explosionPool = Helper.poolManager.eExplosion;
        this.node.emit(EnemyEvent.INIT.toString());

    }
    moveHoz() {
        this.scheduleOnce(() => {
            if (this.node.active) {
                this.node.runAction(cc.sequence(
                    cc.moveBy(0.8, 12, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, -12, 0).easing(cc.easeSineIn()),
                    cc.moveBy(0.8, -12, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, 12, 0).easing(cc.easeSineIn())
                ).repeatForever());
            }
        }, 0.9)
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == 'shipBullet') {
            if (self.node.name == "BossNoel2") {
                if (this.hp <= 149) {
                    G.shootBoss = true;
                }
                this.checkExplosion++;
                if (this.checkExplosion >= 2) {
                    this.spawnExplosion(this.node.x + 70 * (Math.random() * 2 - 1), this.node.y + 50 * (Math.random() * 2 - 1), 0.7);
                    this.checkExplosion = 0;
                }
            
            }
            else if (self.node.name == "miniBoss") {
                // if (this.hp <= 8) {
                //     G.isZigzag = true;
                // }
                // cc.audioEngine.playEffect(G.hitBullet, false);
            }
            this.checkSoundBoss++;
            if (this.checkSoundBoss >= 2) {
                cc.audioEngine.playEffect(G.hitBullet, false);
                this.checkSoundBoss = 0;
            }
            this.onHitShipBullet(other.node);
        }

        // if (other.node.group == 'shipBullet') {
        //     // this.check++;
        //     // if (this.check >= 2) {
        //     //     return false;
        //     // }
        //     // if (G.hitBullet != null)
        //     //     cc.audioEngine.playEffect(G.hitBullet, false);
        //     this.onHitShipBullet(other.node);

        //     // if (this.bar)
        //     //     this.BarHealthy();

        //     G.score += 10;
        // }


        // else if (other.node.group == 'ship') {

        // }
    }

    // BarHealthy() {
    //     this.bar.active = true;
    //     this.bar.children[0].scaleX = this.widthBar * this.hp / this.hpTotal;
    // }

    runExplosionEffect(parent: cc.Node, x: number, y: number, scale: number, speed: number) {
        if (this.explosionPool == null)
            return;
        var explosion = this.explosionPool.createObject(parent);
        explosion.x = x;
        explosion.y = y;
        explosion.scale = scale;
        var clip = explosion.getComponent(cc.Animation).defaultClip;
        clip.speed = speed;
        var t = clip.duration / clip.speed;
        explosion.getComponent(cc.Animation).play();
        explosion.getComponent(cc.Animation).scheduleOnce(function () {
            this.killObject(explosion);
        }.bind(this.explosionPool), t);
    }

    onHitShipBullet(node: cc.Node) {
        node.destroy();
        this.node.emit(EnemyEvent.HIT_BULLET.toString());
        this.onLoseHp();
    }

    runHitBulletEffect() {
        this.node.runAction(cc.sequence(
            cc.moveBy(0.15, 0, 20),
            cc.moveBy(0.15, 0, -20)
        ));
    }

    onLoseHp() {
        this.hp--;
        if (this.hp <= 0) {
            if (this.node.name == 'BossNoel') {
                // cc.audioEngine.playEffect(G.enemyDie, false);
            }
            this._onEnemyDie();
            if (this.node.name == 'miniBoss') {
                this.spawnCoin(7);
            }
            else {
                this.spawnCoin(4);
            }
        }
    }

    private _onEnemyDie() {
        G.score += 10;
        this.node.destroy();
    }

    onDestroy() {
        // this.scheduleOnce(() => {
        //     console.log("destroy!");
        //     if (this.coinList.length != null) {
        //         for (let i = 0; i < this.coinList.length; i++) {
        //             if (this.coinList[i])
        //                 this.coinList[i].destroy();
        //         }
        //     }
        // }, 2)
        G.totalE++;
        this.node.emit(EnemyEvent.DIE.toString());
        this._isDie = true;
        var pos = this.node.parent.convertToNodeSpaceAR(this.node.convertToWorldSpaceAR(this.centrePos));
        this.runExplosionEffect(this.node.parent, pos.x, pos.y + 15, this.scaleExplosion * this.node.scale, this.speedExplosion);
        if (this.onDie)
            this.onDie(this);
    }

    isDie() {
        return this._isDie;
    }

    public spawnExplosion(x: number, y: number, k: number) {
        var newExplosion = this.hitExplosionPool.createObject(cc.Canvas.instance.node);
        newExplosion.setScale(cc.v2(k, k));
        newExplosion.zIndex = 1;
        newExplosion.x = x;
        newExplosion.y = y + 50;
    }
    spawnCoin(k: number) {
        for (let i = 0; i < k; i++) {
            var coinPrefab = cc.instantiate(this.coinPrefab);
            coinPrefab.parent = cc.Canvas.instance.node;
            coinPrefab.x = this.node.x + 20 * (Math.random() * 2 - 1);
            coinPrefab.y = this.node.y + 20 * (Math.random() * 2 - 1);
            this.coinList.push(coinPrefab);
            coinPrefab.runAction(cc.sequence(
                cc.moveTo(4, cc.v2(this.coinNode.x, this.coinNode.y*3)),
                cc.callFunc(() => {
                    coinPrefab.destroy();
                })))
        }
    }

}