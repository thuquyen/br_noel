import ScheduleSkill from "./ScheduleSkill";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu('Skills/MoveDown')
export default class MoveDown extends ScheduleSkill {

    @property(Number)
    speed: number = 500;

    @property(Number)
    deltaSpeed: number = 0;

    private isAttack = false;

    attack(){
        if(!this.node || !this.node.isValid || this.isAttack)
            return;
        this.isAttack = true;
        let x0 = this.node.x;
        let y0 = this.node.y;

        let x1 = x0;
        let y1 = -cc.winSize.height/2;

        let v = this.speed + this.deltaSpeed*Math.random();
        let s = y0-y1;
        let t = s/v;

        this.node.runAction(cc.sequence(
            cc.moveTo(t, x1, y1),
            cc.moveTo(t, x0, y0),
            cc.callFunc(()=>{
                this.isAttack = false;
            })
        ));
    }
    
}