import ESkill from "./ESkill";
import G from "../Global";
import ScheduleSkill from "./ScheduleSkill";
import { EnemyEvent } from "./BaseEnemy";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu('Skills/FollowShip')
export default class FollowShip extends ESkill {

    @property(Number)
    speed: number = 200;

    @property(Number)
    adjustX: number = 0;

    @property(Number)
    adjustY: number = 0;

    @property(Number)
    deltaX: number = 0;

    @property(Number)
    deltaY: number = 0;

    private isAttack: boolean = false;

    start(){
        if((<any>this.node)._appear === false) {
            this.node.on(EnemyEvent.APPEARED.toString(), ()=>{
                this.attack();
            })
        } else {
            this.attack();
        }
    }

    attack() {
        if(!this.node || !this.node.isValid)
            return;
        let x1 = G.ship.node.x + this.adjustX + (Math.random()-0.5)*this.deltaX;
        let y1 = G.ship.node.y + this.adjustY + (Math.random()-0.5)*this.deltaY;
        let s = Math.sqrt((this.node.x-x1) * (this.node.x-x1) + (this.node.y-y1) * (this.node.y-y1));
        let t = s/this.speed;
        this.node.runAction(cc.sequence(
            cc.moveTo(t, x1, y1),
            cc.callFunc(()=>{
                this.attack();
            })
        ));
    }

}