import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property
    r: number = 150;
    // @property
    timeStep: number = 1
    // @property()
    alpha: number = 0;
    @property()
    posX: number = 0;
    @property()
    posY: number = 160;
    // @property()
    time: number = 1;
    // LIFE-CYCLE CALLBACKS:

    start() {
        this.scheduleOnce(() => {
            //get angle
            var dis = {
                'x': this.posX - this.node.position.x,
                'y': this.posY - this.node.position.y
            };
            var angle = Math.atan2(dis.x, dis.y);
            this.alpha = angle;
            if (this.node.activeInHierarchy) {
                this.schedule(() => {
                        this.alpha = this.alpha - cc.misc.degreesToRadians(12);
                        let i = this.posX + (this.r * Math.cos(this.alpha));
                        let j = this.posY + (this.r * Math.sin(this.alpha));
                        this.node.runAction(cc.sequence(
                            cc.moveTo(this.time, cc.v2(i, j)),
                            cc.callFunc(() => {
                            })
                        ))
                }, this.timeStep)
            }
        }, 0.2)
    }

}
