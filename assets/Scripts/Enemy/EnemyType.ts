// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

export enum EnemyType {
    E1 = 1,
    E2 = 2,
    E3 = 3,
    E4 = 4,
    EGold = 5,
    Z1 = 6,
    Z2 = 7,
    Z3 = 8,
    Z4 = 9,
    Z5 = 10,
    Z6 = 11,
    Z7 = 12,
    Z8 = 13,
    Z9 = 14,
    Z10 = 15
}

export class EnemyFactory {

    createEnemy(type: EnemyType){
        
    }

}