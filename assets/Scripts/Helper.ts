import BasePool from "./common/BasePool";
import PoolManager from "./PoolManager";
import G from "./Global";

export class Helper {

    public static getSequencePoints(node: cc.Node){
        var points = [];
        //node.children[0].position = cc.v2(100, 100);
        for(var i=0; i<node.children.length; i++) {
            points.push(node.children[i].getPosition());
        }
        return points;
    }

    public static moveTo(target: cc.Node, endPoint: cc.Vec2, speed: number, onComplete: Function){
        var d = Math.sqrt((target.x-endPoint.x)*(target.x-endPoint.x) + (target.y-endPoint.y)*(target.y-endPoint.y));
        var t = d/speed;
        target.runAction(cc.sequence(
            cc.moveTo(t, endPoint).easing(cc.easeSineOut()),
            cc.callFunc(()=>{
                if(onComplete)
                    onComplete();
            })
        ));
    }

    public static moveToAndAdjustAngle(target: cc.Node, endPoint: cc.Vec2, speed: number, onComplete: Function){
        var d = Math.sqrt((target.x-endPoint.x)*(target.x-endPoint.x) + (target.y-endPoint.y)*(target.y-endPoint.y));
        var t = d/speed;
        target.runAction(cc.sequence(
            cc.spawn(
                cc.moveTo(t, endPoint).easing(cc.easeSineOut()),
                cc.rotateTo(0.2, 0)
            ),
            cc.callFunc(()=>{
                if(onComplete)
                    onComplete();
            })
        ));
    }

    public static splineFollow(target: cc.Node, points: cc.Vec2[], speed: number, delay: number, angleOffset: number, onComplete: Function){
        this._splineOnlyFollow(points, target, speed, delay, angleOffset, onComplete);
    }

    public static splineFollowLoop(target: cc.Node, points: cc.Vec2[], speed: number, delay: number, angleOffset: number){
        this._splineOnlyFollowLoop(points, target, speed, delay, angleOffset);
    }

    public static splineFollowAndRotate(target: cc.Node, points: cc.Vec2[], speed: number, delay: number, angleOffset: number, onComplete: Function){
        this._splineFollowAndRotate(points, target, speed, delay, angleOffset, onComplete);
    }

    public static splineFollowAndRotateLoop(target: cc.Node, points: cc.Vec2[], speed: number, delay: number, angleOffset: number){
        this._splineFollowAndRotateLoop(points, target, speed, delay, angleOffset);
    }

    public static splineFollowAndRotate2(target: cc.Node, points: cc.Vec2[], speed: number, delay: number, angleOffset: number, onComplete: Function){
        var ps = points.slice(0);
        var endPoint = ps.pop();
        this._splineFollowAndRotate(ps, target, speed, delay, angleOffset, ()=>{
            Helper.moveToAndAdjustAngle(target, endPoint, speed/1.5, onComplete);
        })
    }

    public static splineFollowAndRotate3(target: cc.Node, ps: cc.Vec2[], endPoint: cc.Vec2, speed: number, delay: number, angleOffset: number, onComplete: Function){
        this._splineFollowAndRotate(ps, target, speed, delay, angleOffset, ()=>{
            Helper.moveToAndAdjustAngle(target, endPoint, speed/1.5, onComplete);
        })
    }

    private static _splineFollowAndRotate(points: cc.Vec2[], target: cc.Node, speed: number, delay: number, angleOffset: number, onComplete: Function){
        if(delay) {
            setTimeout(()=>{
                if(target && target.isValid) {
                    target.runAction(cc.sequence(
                        cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1),
                        cc.callFunc(()=>{
                            if(onComplete)
                                onComplete();
                        })
                    ));
                }
            }, delay*1000);
        } else {
            target.runAction(cc.sequence(
                cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1),
                cc.callFunc(()=>{
                    if(onComplete)
                        onComplete();
                })
            ));
        }
    }

    private static _splineOnlyFollow(points: cc.Vec2[], target: cc.Node, speed: number, delay: number, angleOffset: number, onComplete: Function){
        if(delay) {
            setTimeout(()=>{
                if(target && target.isValid) {
                    let action: cc.CardinalSplineTo2 = cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1);
                    action.disableRotate();
                    target.runAction(cc.sequence(
                        action,
                        cc.callFunc(()=>{
                            if(onComplete)
                                onComplete();
                        })
                    ));
                }
            }, delay*1000);
        } else {
            let action: cc.CardinalSplineTo2 = cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1);
            action.disableRotate();
            target.runAction(cc.sequence(
                action,
                cc.callFunc(()=>{
                    if(onComplete)
                        onComplete();
                })
            ));
        }
    }

    private static _splineFollowAndRotateLoop(points: cc.Vec2[], target: cc.Node, speed: number, delay: number, angleOffset: number){
        if(delay) {
            setTimeout(()=>{
                if(target && target.isValid) {
                    target.runAction(cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1).repeatForever())
                }
            }, delay*1000);
        } else {
            target.runAction(cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1).repeatForever());
        }
    }

    private static _splineOnlyFollowLoop(points: cc.Vec2[], target: cc.Node, speed: number, delay: number, angleOffset: number){
        if(delay) {
            setTimeout(()=>{
                if(target && target.isValid) {
                    let action: cc.CardinalSplineTo2 = cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1);
                    action.disableRotate();
                    target.runAction(action.repeatForever());
                }
            }, delay*1000);
        } else {
            let action: cc.CardinalSplineTo2 = cc.cardinalSplineTo2_speed(speed, points, angleOffset, 0.1);
            action.disableRotate();
            target.runAction(action.repeatForever());
        }
    }

    public static runShowAnim(onDone: Function = null){
        if(onDone)
            onDone();
    }


    public static runHideAnim(node: cc.Node, onDone: Function = null){
        let anim = node.getComponent(cc.Animation);
        if(anim) {
            let state = anim.play('hide');
            anim.scheduleOnce(()=>{
                node.active = false;
                if(onDone)
                    onDone();
            }, state.duration/state.speed);    
        }
    }

    public static runAnim(node: cc.Node, animName: string, onDone: Function = null){
        let anim = node.getComponent(cc.Animation);
        if(anim) {
            let state = anim.play(animName);
            anim.scheduleOnce(()=>{
                if(onDone)
                    onDone();
            }, state.duration/state.speed);    
        }
    }

    public static poolManager: PoolManager = null;

}