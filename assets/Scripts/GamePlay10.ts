import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./ShipController";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(WaveQuangCao)
    wave1: WaveQuangCao = null;

    @property(WaveQuangCao)
    wave2: WaveQuangCao = null;

    @property(ShipController)
    ship: ShipController = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Node)
    winPopup: cc.Node = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;
    @property(cc.Node)
    bar: cc.Node = null;

    // @property(cc.Node)
    // textExcerllent: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    @property(cc.Prefab)
    itemChangeShip: cc.Prefab[] = [];

    index: number = 1;

    nShip: number = 1;
    nextWave1: boolean = false;
    end: boolean = false;
    @property(cc.Node)
    logo: cc.Node = null;
    @property(cc.Node)
    ring: cc.Node = null;
    @property(cc.Node)
    heart: cc.Node = null;
    private timeDelay: number = 0.2;
    private isWaveBoss: boolean = false;
    // @property(cc.Node)
    // btnInstall: cc.Node = null;
    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;
    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        // this.buttonInstall.zIndex = 3;
        this.hand.node.zIndex = 4;
        this.winPopup.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        // this.updateItem();
        this.updateChangeShip();
        // this.wave1.appear(() => {
        // });
        // this.scheduleOnce(() => {
        //     this.wave2.appear(() => {
        //     });
        // }, this.timeDelay)
        // this.scheduleOnce(() => {
        //     this.wave3.appear(() => {
        //     });
        // }, this.timeDelay * 1.5)
        // this.scheduleOnce(() => {
        //     this.wave4.appear(() => {
        //     });
        // }, this.timeDelay * 1.5)
        // this.scheduleOnce(() => {
        //     this.wave5.appear(() => {
        //     });
        // }, this.timeDelay * 2)

        // this.scheduleOnce(() => {
        //     this.wave6.appear(() => {
        //     });
        // }, this.timeDelay * 2)


        Helper.runShowAnim(() => {
            this.ship.node.runAction(cc.moveBy(0.5, 0, 350).easing(cc.easeBackOut()));
            this.scheduleOnce(() => {
                this.gameState = GameState.READY;
                var pos = this.ship.node.getPosition();
                this.hand.runSwipeAnim(pos.x + 30, pos.y - 50);
            }, 0.5);
        });

        // this.wave1.onComplete = () => {
            // this.textExcerllent.active = true;
            // this.textExcerllent.runAction(cc.spawn(cc.fadeIn(0.2), cc.scaleBy(0.2, 6).easing(cc.easeBackOut())));
            // this.scheduleOnce(() => {s
            //     this.textExcerllent.runAction(cc.spawn(cc.fadeOut(0.2), cc.scaleBy(0.2, 1 / 6).easing(cc.easeBackIn())));
            // }, 0.5);
        //     G.totalWave++;
        // }
        // this.wave2.onComplete = () => {
        //     G.totalWave++;
        // }
        // this.wave3.onComplete = () => {
        //     G.totalWave++;
        // }
        // this.wave4.onComplete = () => {
        //     G.totalWave++;
        // }
        // this.wave5.onComplete = () => {
        //     G.totalWave++;
        // }
      
        this.ship.onDie = () => {
            this.scheduleOnce(() => {
                this.showWinPopup();
            }, 0.5);
        }
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                break;
        }
    }

    startGame() {
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
        this.ship.setEnableBullet(true);
        this.wave1.startWave();
        G.clickStart = true;
    }

    showWinPopup() {
        // cc.audioEngine.stop(this.temp);
        // this.btnInstall.opacity = 0;
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;
        // this.logo.opacity = 0;
        // this.ring.opacity = 0;
        this.heart.opacity = 0;
        // this.buttonInstall.active = false;
        // this.bar.opacity = 0;
        // cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        G.isEnd = true;
    }

    showLosePopup() {
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;

        // this.buttonInstall.active = false;
        // cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        this.winPopup.opacity = 0;
        setTimeout(() => {
            this.winPopup.opacity = 255;
        }, 1);
        this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_lose', false);
        this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
            this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_lose', true);
            this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
        });
    }

    spawnItem() {
        var newItem = cc.instantiate(this.item);
        newItem.parent = this.node;
        newItem.zIndex = this.index;
        newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
        newItem.y = (this.node.y) * Math.random();
    }

    updateItem() {
        this.schedule(() => {
            if (G.stateItem)
                this.spawnItem();
        }, 5, cc.macro.REPEAT_FOREVER, 2);
    }

    changeShip() {
        let prefabShip = this.itemChangeShip[this.nShip];
        this.nShip++;
        if (this.nShip >= this.itemChangeShip.length)
            this.nShip = 0;
        var newItem = cc.instantiate(prefabShip);
        newItem.parent = this.node;
        newItem.x = (this.node.x - 150) * (2 * Math.random() - 1);
        newItem.y = (this.node.y) * Math.random();
        newItem.scale = 0.2;
        newItem.runAction(cc.spawn(cc.scaleTo(0.2, 1), cc.moveBy(10, cc.v2(0, -1000))));
    }

    updateChangeShip() {
        this.schedule(() => {
            this.changeShip();
        }, 6, cc.macro.REPEAT_FOREVER, 5);
    }
    update(dt) {
        // if (G.totalWave >= 4 && !this.isWaveBoss) {
        //     this.isWaveBoss = true;
        //     this.wave5.appear(() => {
        //     });
        // }
        // if (G.totalE >= 1 && !this.nextWave1) {
        //     this.nextWave1 = true;
        //     this.wave2.appear(() => {
        //     });
        // }
        if (G.totalWave >= 5 && !this.end) {
            this.gameState = GameState.ENDGAME;
            this.end = true;
            this.ship.flyOut(() => {
                this.showWinPopup();
            });
        }
        if (G.clickStart) {
            G.clickStart = false;
            this.updateItem();
            this.updateChangeShip();

            // this.scheduleOnce(()=> {
            //     this.gameState = GameState.ENDGAME;
            //     this.end = true;
            //     this.ship.flyOut(() => {
            //         this.showWinPopup();
            //     });
            // },20)
        }

    }
}