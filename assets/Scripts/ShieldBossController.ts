// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ShieldBossController extends cc.Component {

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if(other.node.group == 'shipBullet') {
            this.onHitShipBullet(other.node);
        }
    }

    onHitShipBullet(node: cc.Node){
       node.destroy(); 
    }

    showShield(){
        this.node.scale = 0;
        this.node.active = true;
        this.node.runAction(cc.sequence(
            cc.scaleTo(0.8, 2.5, -2.5).easing(cc.easeBackOut()),
            cc.callFunc(()=>{
                this.node.runAction(cc.repeatForever(
                    cc.sequence(
                        cc.scaleTo(0.5, 2.6, -2.6),
                        cc.scaleTo(0.5, 2.5, -2.5)
                    )
                ));
            })
        ));
    }

    hideShield(){
        this.node.stopAllActions();
        this.node.runAction(cc.sequence(
            cc.spawn(
                cc.scaleTo(0.5, 5, -5).easing(cc.easeBackIn()),
                cc.fadeOut(0.5).easing(cc.easeBackIn())
            ),
            cc.callFunc(()=>{
                this.node.active = false;
            })
        ));
    }

}