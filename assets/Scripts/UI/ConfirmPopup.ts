// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ConfirmPopup extends cc.Component {

    @property(cc.Label)
    content: cc.Label = null;

    @property(cc.Label)
    title: cc.Label = null;

    @property(cc.Button)
    confirmButton: cc.Button = null;

    public static onHide: Function = null;

    private static _instance: ConfirmPopup = null;

    public static get isShow(){
        return (this._instance && this._instance.node.active);
    }

    public static get instantiated(){
        return this._instance != null;
    }

    onLoad(){
        this.confirmButton.node.on('click', ()=>{
            ConfirmPopup.hidePopup();
        });
    }

    public static showPopup(title, content, onDone: Function = null){
        if(this._instance === null) {
            cc.loader.loadRes("ui/ConfirmPopup", (err, prefab) => {
                var newNode: cc.Node = cc.instantiate(prefab);
                cc.Canvas.instance.node.addChild(newNode);
                this._instance = newNode.getComponent(ConfirmPopup);
                this._instance.title.string = title;
                this._instance.content.string = content;
                newNode.x = 0;
                newNode.y = cc.winSize.height/2 + this._instance.node.getContentSize().height/2;
                newNode.runAction(cc.sequence(
                    cc.moveTo(0.5, 0, 0).easing(cc.easeElasticOut(10)),
                    cc.callFunc(()=>{
                        if(onDone) onDone();
                    })
                ));
            });
        } else {
            this._instance.node.active = true;
            this._instance.title.string = title;
            this._instance.content.string = content;
            this._instance.node.x = 0;
            this._instance.node.y = cc.winSize.height/2 + this._instance.node.getContentSize().height/2;
            this._instance.node.runAction(cc.sequence(
                cc.moveTo(0.5, 0, 0).easing(cc.easeElasticOut(10)),
                cc.callFunc(()=>{
                    if(onDone) onDone();
                })
            ));
        }
    }

    public static hidePopup(onDone: Function = null) {
        if(this._instance) {
            var yOut = cc.winSize.height/2 + this._instance.node.getContentSize().height/2;
            this._instance.node.runAction(cc.sequence(
                cc.moveTo(0.25, 0, yOut).easing(cc.easeSineIn()),
                cc.callFunc(()=>{
                    this._instance.node.active = false;
                    if(onDone) onDone();
                    if(this.onHide)
                        this.onHide();
                })
            ));
        }
    }

}