// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

export enum DIRECTION {
    LEFT = 0,
    RIGHT = 1
}

@ccclass
export default class MoveLeftRight extends cc.Component {

    @property(Number)
    speed: number = 200;

    @property(Number)
    leftBorder: number = -300;

    @property(Number)
    rightBorder: number = 300;

    @property(Number)
    y: number = 350;

    direction: DIRECTION = DIRECTION.LEFT;
    
    update(dt){
        if(this.direction == DIRECTION.LEFT) {

        } else if(this.direction == DIRECTION.RIGHT) {

        }
    }

}