import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    @property()
    private enableBullet: boolean = true;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 200;

    private fireTime = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    @property(cc.Node)
    tim: cc.Node = null;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    private hp = 0;
    // @property(HPBar)
    // public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    @property(cc.Prefab)
    bullet11: cc.Prefab[] = [];

    @property(cc.Prefab)
    bullet14: cc.Prefab[] = [];


    bullet: cc.Prefab[] = [];

    // @property(cc.Prefab)
    // fxItem: cc.Prefab = null;

    @property(sp.SkeletonData)
    changeShip: sp.SkeletonData[] = [];

    @property(cc.Node)
    shieldEffect: cc.Node = null;
    @property(cc.Node)
    swing: cc.Node = null;

    onLoad() {
        this.bulletPool.prefab = this.bullet14[0];
        this.bullet = this.bullet14;
        // this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    update(dt) {
        if (this.enableBullet) {
            this.updateBullet();
        }

        if (this.enableMove) {
            this.updateMove();
        }
        // if (G.text) {
        //     this.enableProtect = true;
        // }
        // else{
        //     this.enableProtect = false;
        // }

    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireTime = Date.now();
            this.fireBullet();
        }
    }

    private fireBullet() {
        if (G.e === false) {
            // cc.audioEngine.playEffect(G.enemyShoot, false);
            var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
            newBulletShip.x = this.node.x;
            newBulletShip.y = this.node.y + 60;
        } else {
            return;
        }
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == "item") {
            if (other.node.name == 'item_Upgrade') {
                this.itemUpgrade(other);
                this.isSwing();
            }
            if (other.node.name == 'ItemShip11') {
                this.itemChangeShip11(other);
                this.bullet = this.bullet14;
            }

            if (other.node.name == 'ItemShip14') {
                this.itemChangeShip14(other);
                this.bullet = this.bullet11;
            }

            this.changeBulletShip(this.bullet);
            if (!G.isEnd) {
                cc.audioEngine.playEffect(G.powerUp, false);
            }
        }

        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'bossBullet') {
            // if (G.hitBullet != null)
            //     cc.audioEngine.playEffect(G.hitBullet, false);
            this.onHitBossBullet(other.node);
        }
        else if (other.node.group == 'e') {
            // if (G.hitBullet != null ){
            //     cc.audioEngine.playEffect(G.hitBullet, false);
            //     this.numberSound = 0;
            // }
            this.onHitEnemy(other.node);
        }
        else if (other.node.group == 'eBullet') {
            this.onHitEnemyBullet(other.node);
        }
    }

    itemChangeShip11(other: cc.Collider) {
        this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[0];
        this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
        // this.node.getComponent(sp.Skeleton).setSkin('skin2');
        this.node.setScale(cc.v2(0.85, 0.85));
        other.node.destroy();
    }

    itemChangeShip14(other: cc.Collider) {
        this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[1];
        this.node.getComponent(sp.Skeleton).setSkin('skin2');
        this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
        this.node.setScale(cc.v2(0.85, 0.85));
        other.node.destroy();
    }

    itemChangeShip16(other: cc.Collider) {
        this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[2];
        this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
        this.node.setScale(cc.v2(0.73, 0.73));
        other.node.destroy();
    }

    itemUpgrade(other: cc.Collider) {
        G.bulletUpgrade++;
        G.score += 10;
        other.node.destroy();
        // this.spawnFxItem();
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }


    private loseHp(n: number) {
        G.bulletDie = true;
        G.lazeShip = false;
        this.hp -= n;
        // if (this.hpBar) {
        //     this.hpBar.setPercent(this.hp / this.HPMAX);
        // }
        this.showLife(this.hp);
        if (this.hp <= 0) {
            if (this.hackNoDie) {
                this.hp = this.HPMAX;
                // this.hpBar.setPercent(this.hp / this.HPMAX);
            } else {
                this.hp = 0;
                this.onShipDie();
            }
        }
    }

    private onShipDie() {
        this.isDie = true;
        G.isShipDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }

    showLife(n: number) {
        cc.audioEngine.playEffect(G.flyDie, false);
        this.explosion();
        this.node.opacity = 255;
        if (this.tim.children[n].isValid) {
            this.tim.children[n].opacity = 0;
        }
        this.node.setPosition(cc.v2(0, -598));
        this.shieldEffect.opacity = 255;
        // this.shieldEffect.runAction(cc.spawn(cc.fadeIn(0.2), cc.scaleTo(0.2, 1)));
        this.enableProtect = true;
        G.touchPos = null;
        this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(0, -293)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.getComponent(cc.CircleCollider).enabled = true;
            this.scheduleOnce(() => {
                this.enableProtect = false;
                this.shieldEffect.opacity = 0;
                // this.shieldEffect.runAction(cc.spawn(cc.fadeOut(0.3), cc.scaleTo(0.3, 0)));
            }, 2.5)
            this.enableBullet = true;
            G.touchPos = null;
            this.enableMove = true;
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        this.enableBullet = true;
        this.enableMove = true;
        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 2.5);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }

    changeBulletShip(prefabs) {
        switch (G.bulletUpgrade) {
            case 1: {
                this.bulletPool.prefab = prefabs[0];
                break;
            }

            case 2: {
                this.bulletPool.prefab = prefabs[1];
                break;
            }

            case 3: {
                this.bulletPool.prefab = prefabs[2];
                G.checkWing = true;
                G.stateItem = false;
                // G.lazeShip = true;
                break;
            }

            default: {
                this.bulletPool.prefab = prefabs[2];
                break;
            }
        }
    }
    isSwing() {
        this.swing.opacity = 255;
        this.swing.getComponent(cc.Animation).getAnimationState("wings").play();
        this.scheduleOnce(() => {
            this.swing.opacity = 0;
            this.swing.getComponent(cc.Animation).getAnimationState("wings").pause();
        }, 2);
    }

    // spawnFxItem() {
    //     var newFxItem = cc.instantiate(this.fxItem);
    //     newFxItem.parent = cc.Canvas.instance.node;
    //     newFxItem.x = this.node.x;
    //     newFxItem.y = this.node.y;
    // }
}