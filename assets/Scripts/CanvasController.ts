import G from "./Global";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class CanvasController extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    anim: cc.Animation;

    // @property(cc.Prefab)
    // winPopupPrefab: cc.Prefab = null;

    @property(cc.Node)
    installToPlay: cc.Node = null;

    @property(cc.Node)
    tapAndHoldToPlay: cc.Node = null;

    appear(onDone: Function){
        this.anim = this.getComponent(cc.Animation);
        this.anim.play();
        this.scheduleOnce(()=>{
            if(onDone) {
                onDone();
            }
        }, this.anim.currentClip.duration);
    }

    hideStoryText(){
        this.anim = this.getComponent(cc.Animation);
        this.anim.play("hideStoryText");
    }

    // showWinPopup(){
    //     var winPopup = cc.instantiate(this.winPopupPrefab);
    //     winPopup.parent = this.node;
    //     winPopup.opacity = 0;
    //     setTimeout(()=>{
    //         winPopup.opacity = 255;
    //     }, 1);
    //     winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_win', false);
    //     winPopup.getComponent(sp.Skeleton).setCompleteListener(()=>{
    //         winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_win', false);
    //     });
    // }

    // hideInstallToPlay(){
    //     this.installToPlay.runAction(cc.fadeOut(0.2));
    // }

}