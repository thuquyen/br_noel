import BasePool from "./common/BasePool";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    @property()
    public timeFire: number = 0;

    @property()
    ratioScaleBullet: number = 0;

    onLoad() {
        for (let i = 0; i < this.bullet.length; i++) {
            this.bulletShip(this.bullet[i]);
        }
    }

    bulletShip(fire: cc.Node) {
        fire.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000)), cc.scaleBy(0.2, this.ratioScaleBullet)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    // update (dt) {}
}
