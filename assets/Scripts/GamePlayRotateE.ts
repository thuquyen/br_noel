import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./ShipController";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

@ccclass
export default class GamePlayRotate extends cc.Component {

    gameState: number = GameState.INIT;

    @property(ShipController)
    ship: ShipController = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Node)
    winPopup: cc.Node = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;
    @property(cc.Node)
    bar: cc.Node = null;

    @property(cc.Node)
    textExcerllent: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    @property(cc.Prefab)
    itemChangeShip: cc.Prefab[] = [];

    index: number = 1;

    nShip: number = 1;
    nextWave1: boolean = false;
    end: boolean = false;
    @property(cc.Node)
    logo: cc.Node = null;
    @property(cc.Node)
    ring: cc.Node = null;
    @property(cc.Node)
    heart: cc.Node = null;
    private timeDelay: number = 0.2;
    private isWaveBoss: boolean = false;

    // @property(cc.Node)
    // btnInstall: cc.Node = null;
    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;
    @property(cc.Node)
    nextLevel: cc.Node = null;
    @property(cc.Node)
    holdToShoot: cc.Node = null;
    @property(cc.Node)
    endNode: cc.Node = null;
    @property(cc.Node)
    itemNextLevel: cc.Node = null;
    isSpawnItem: boolean = false;
    // onLoad(){
    //     // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
    //     cc.game.on(cc.game.EVENT_HIDE, () => {
    //         cc.audioEngine.stop(this.temp);
    //     })
    //     cc.game.on(cc.game.EVENT_SHOW, () => {
    //         this.temp = cc.audioEngine.play(this.bgSound, true, 1);
    //     })
    // }
    start() {
        // this.buttonInstall.zIndex = 3;
        this.hand.node.zIndex = 4;
        this.winPopup.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        // this.updateItem();
        // this.updateChangeShip();
        Helper.runShowAnim(() => {
            this.ship.node.runAction(cc.moveBy(0.5, 0, 350).easing(cc.easeBackOut()));
            this.scheduleOnce(() => {
                this.gameState = GameState.READY;
                var pos = this.ship.node.getPosition();
                this.hand.runSwipeAnim(this.holdToShoot.x, this.holdToShoot.y - 100);
            }, 0.5);
        });

        this.ship.onDie = () => {
            this.scheduleOnce(() => {
                this.showWinPopup();
                this.buttonInstall.opacity = 0;
            }, 0.5);
        }
    }

    onTouchStart(event: cc.Touch) {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    this.hand.runSwipeAnim(this.holdToShoot.x, this.holdToShoot.y - 100);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                break;
        }
    }

    startGame() {
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
        this.ship.setEnableBullet(true);
        G.clickStart = true;
    }

    showWinPopup() {

        // this.btnInstall.opacity = 0;
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;
        // this.logo.opacity = 0;
        // this.ring.opacity = 0;
        this.heart.opacity = 0;
        // this.buttonInstall.active = false;
        // this.bar.opacity = 0;
        // cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        G.isEnd = true;
    }

    showLosePopup() {
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;

        // this.buttonInstall.active = false;
        // cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        this.winPopup.opacity = 0;
        setTimeout(() => {
            this.winPopup.opacity = 255;
        }, 1);
        this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_lose', false);
        this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
            this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_lose', true);
            this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
        });
    }

    spawnItem() {
        if (!this.isSpawnItem) {
            var newItem = cc.instantiate(this.item);
            newItem.parent = this.node;
            // newItem.zIndex = this.index;
            newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
            newItem.y = (this.node.y) * Math.random() - 100;
        }
    }

    updateItem() {
        this.schedule(() => {
            if (G.stateItem)
                this.spawnItem();
        }, 4, cc.macro.REPEAT_FOREVER, 0.01);
    }

    changeShip() {
        if (!this.isSpawnItem) {
            let prefabShip = this.itemChangeShip[this.nShip];
            this.nShip++;
            if (this.nShip >= this.itemChangeShip.length)
                this.nShip = 0;
            var newItem = cc.instantiate(prefabShip);
            newItem.parent = this.node;
            newItem.x = (this.node.x - 150) * (2 * Math.random() - 1);
            newItem.y = (this.node.y) * Math.random();
            newItem.scale = 0.2;
            newItem.runAction(cc.spawn(cc.scaleTo(0.2, 1), cc.moveBy(10, cc.v2(0, -1000))));
        }
    }

    updateChangeShip() {
        this.schedule(() => {
            this.changeShip();
        }, 6, cc.macro.REPEAT_FOREVER, 2);
    }
    endG() {
        G.bg = 100;
        this.textExcerllent.zIndex = 3;
        this.nextLevel.zIndex = 3;
        this.hand.node.active = false;
        this.holdToShoot.active = false;
        this.isSpawnItem = true;
        this.end = true;
        G.e = true;
        this.ship.node.active = false;
        this.buttonInstall.opacity = 0;
        G.text = false;

        this.scheduleOnce(() => {
            this.itemNextLevel.active = true;
        }, 1.6)
        this.scheduleOnce(() => {
            this.textExcerllent.active = true;
        }, 0.2)

        this.scheduleOnce(() => {
            this.itemNextLevel.opacity = 0;
            this.nextLevel.active = true;
            this.endNode.active = true;
        }, 3.2)
    }

    update(dt) {
        if (G.clickStart) {
            G.clickStart = false;
            this.updateItem();
            this.updateChangeShip();
            // this.scheduleOnce(()=> {
            //     this.gameState = GameState.ENDGAME;
            //     this.end = true;
            //     this.ship.flyOut(() => {
            //         this.showWinPopup();
            //     });
            // },20)
        }
        if (G.totalE == 13) {
            this.endG();
        }
    }
}