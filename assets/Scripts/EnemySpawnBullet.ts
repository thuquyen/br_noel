import BaseEnemy from "./Enemy/BaseEnemy";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class EnemySpawnBullet extends BaseEnemy {

    @property(cc.Prefab)
    prefabBullet: cc.Prefab = null;

    spawnBullet(){
        var eBullet = cc.instantiate(this.prefabBullet);
        eBullet.parent = this.node.parent;
        eBullet.x = this.node.x;
        eBullet.y = this.node.y;
        
        eBullet.runAction(cc.sequence(
            cc.moveBy(6, 0, -1500),
            cc.callFunc(()=>{
                eBullet.destroy();
            })
        ));
        
        this.scheduleOnce(()=>{
            eBullet.zIndex = 2;
        }, 100);
    }

    @property()
    fireRate = 5;

    @property()
    fireDelay = 1;

    startAttack(){
        this.schedule(()=>{
            this.spawnBullet();
        }, this.fireRate, cc.macro.REPEAT_FOREVER, this.fireDelay);
    }



}