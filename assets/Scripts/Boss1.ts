import BaseBoss from "./Boss/BaseBoss";
import BossState from "./BossState";
import WaveController from "./WaveController";
import ShieldBossController from "./ShieldBossController";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Boss1 extends BaseBoss {

    @property(cc.Node)
    ship: cc.Node = null;

    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;

    @property()
    animIdle: string = 'idle';

    @property()
    animAttack: string = 'attack';

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        super.onLoad();
    }

    @property(ShieldBossController)
    shieldBoss: ShieldBossController = null;

    startAttack(){
        super.startAttack();
        this.shieldBoss.hideShield();
        this.hpBar.appear();
        this.scheduleOnce(()=>{
            this.moveAround();
            this.scheduleAttack();
            this.enableBullet = true;
            this.moveToShip();
        }, 0.5);
    }

    _moveAroundAction: cc.Action = null;
    moveAround(){
        this.getComponent(cc.Animation).play('bossMoveAround');
    }

    timeMoveToShip: number = 5;
    variantTimeMoveToShip: number = 2;

    scheduleAttack(){
        if(this.bossState == BossState.DIE)
            return;
        this.scheduleOnce(()=>{
            this.moveToShip();
            this.scheduleAttack();
        }, this.timeMoveToShip + this.variantTimeMoveToShip*Math.random());
    }

    pauseMoveAround(){
        this.getComponent(cc.Animation).pause();
    }

    resumeMoveAround(){
        this.getComponent(cc.Animation).resume();
    }

    @property(cc.Vec2)
    adjustShipPos: cc.Vec2 = new cc.Vec2(0, -300);

    moveToShip(){
        this.getComponent(sp.Skeleton).setMix(this.animIdle, this.animAttack, 0.06);
        this.getComponent(sp.Skeleton).setAnimation(0, this.animAttack, true);
        this.getComponent(sp.Skeleton).setCompleteListener(()=>{
            this.getComponent(sp.Skeleton).setMix(this.animAttack, this.animIdle, 0.06);
            this.getComponent(sp.Skeleton).setAnimation(0, this.animIdle, true);
        });
        this.pauseMoveAround();
        this.enableBullet = false;
        var x0 = this.node.x;
        var y0 = this.node.y;
        this.node.runAction(cc.sequence(
            cc.moveTo(0.8, this.ship.x + this.adjustShipPos.x, this.ship.y + this.adjustShipPos.y),
            cc.moveTo(0.8, x0, y0),
            cc.callFunc(()=>{
                this.getComponent(sp.Skeleton).setAnimation(0, this.animIdle, true);
                this.resumeMoveAround();
                this.enableBullet = true;
            })
        ));
    }

    fireBullet(){
        var bullet = cc.instantiate(this.bulletPrefab);
        bullet.parent = this.node.parent;
        bullet.x = this.node.x;
        bullet.y = this.node.y + 20;
        bullet.runAction(cc.sequence(
            cc.moveBy(2, 0, -1000),
            cc.callFunc(()=>{
                bullet.destroy()
            })
        ));
    }

    enableBullet: boolean = false;
    timeBullet: number = 0;
    @property()
    fireRate: number = 400;

    update(dt: number){
        if(this.enableBullet) {
            var now = Date.now();
            if(this.timeBullet + this.fireRate < now) {
                this.timeBullet = now;
                this.fireBullet();
            }
        }
    }

}