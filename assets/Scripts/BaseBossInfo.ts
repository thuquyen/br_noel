import BaseBoss from "./Boss/BaseBoss";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseBossInfo extends cc.Component {

    @property(cc.Prefab)
    bossPrefab: cc.Prefab = null;

    @property(Number)
    HPMAX: number = 100;

    spawnBoss(): cc.Node {
        var boss = cc.instantiate(this.bossPrefab);
        boss.getComponent(BaseBoss).HPMAX = this.HPMAX;
        return boss;
    }
    
}
