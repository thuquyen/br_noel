import IWave from "../Wave/IWave";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class LevelBR extends cc.Component {

    public onComplete: Function;

    @property([cc.Prefab])
    public wavePrefabs: cc.Prefab[] = [];

    @property([cc.Vec2])
    startPath: cc.Vec2[] = [];

    @property(cc.Color)
    color: cc.Color = cc.color(255, 255, 255, 255);

    startLevel(){
        this.initWave(0);
    }

    initWave(i: number) {
        let waveNode = cc.instantiate(this.wavePrefabs[i]);
        waveNode.parent = cc.Canvas.instance.node;
        let wave = waveNode.getComponent(IWave);
        wave.startWave();
        if(i === this.wavePrefabs.length-1) {
            wave.onComplete = ()=>{
                if(this.onComplete)
                    this.onComplete();
            }
        } else {
            wave.onComplete = ()=>{
                this.initWave(i+1);
            }
        }
    }

}