import G from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScoreLabelBR8 extends cc.Component {

    // @property(cc.Label)
    // scoreLabel: cc.Label = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    public PauseLabel: cc.Node = null;

    @property(cc.Label)
    score: cc.Label = null;

    // @property(cc.Node)
    // laze: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    stateLaze: boolean = true;

    stateStart: boolean = false;
    @property(cc.Node)
    ring: cc.Node = null;

    start() {

        // this.logo.zIndex = 3;
        // this.ring.zIndex = 3;
        // this.laze.zIndex = 0;
        // this.wing.zIndex = 2;
        // if (this.FxPow)
        //     this.FxPow.zIndex = 2;
        this.PauseLabel.zIndex = 2;
        // cc.audioEngine.playMusic(G.bgSound, true);
        G.bg = 160;
        this.bgLayout.node.zIndex = 1;
        this.node.on("touchstart", () => {
            if (G.check) {
                G.bg = 160;
                // G.text = false;
                G.e = true;
            } else {
                this.stateStart = true;
                G.bg = 0;
                G.text = false;
                G.e = false;
                G.fire = 1000;
                // if (G.lazeShip) {
                //     this.laze.active = true;
                //     this.laze.runAction(cc.fadeIn(0.2));
                // }
                // else {
                //     this.laze.runAction(cc.fadeOut(0.1));
                // }
            }
            return;
        });

        this.node.on("touchend", () => {
            if (G.check) {
                G.bg = 160;
                G.text = false;
                G.e = true;
            } else {
                this.stateStart = false;
                G.bg = 160;
                G.text = true;
                G.e = true;
                G.fire = 1;
                // this.laze.runAction(cc.fadeOut(0.1));
            }
            return;
        });
    }

    update(dt) {

        if (G.lazeShip && this.stateLaze && this.stateStart) {
            this.stateLaze = false;
            // this.laze.active = true;
            // this.laze.runAction(cc.fadeIn(0.2));
        }

        // this.scoreLabel.string = "SCORE: " + G.score.toString();
        if (this.score != null)
            this.score.string = G.score.toString();

        this.bgLayout.node.opacity = G.bg;
        this.PauseLabel.active = G.text;

        // if (G.checkWing) {
        //     if (this.FxPow)
        //         this.FxPow.active = true;
        //     this.wing.active = true;

        // } else {
        //     if (this.FxPow)
        //         this.FxPow.active = false;
        //     this.wing.active = false;

        if (G.check) {
            // this.scoreLabel.node.active = false;
            G.bg = 160;
            G.text = false;
            G.e = true;
        }
        if (G.text) {
            G.enableBulletBoss = false;
        }
        else {
            G.enableBulletBoss = true;
        }
    }

}
