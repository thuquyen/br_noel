// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Node)
    p1: cc.Node = null;

    @property(cc.Node)
    p2: cc.Node = null;

    @property(cc.Node)
    p3: cc.Node = null;

    @property(cc.Node)
    p4: cc.Node = null;

    @property(cc.Node)
    p5: cc.Node = null;

    @property(cc.Node)
    p6: cc.Node = null;

    @property(cc.Node)
    p7: cc.Node = null;

    @property(cc.Node)
    p8: cc.Node = null;

    @property(cc.Node)
    e1: cc.Node = null;

    @property(cc.Node)
    e2: cc.Node = null;

    @property(cc.Node)
    e3: cc.Node = null;

    @property(cc.Node)
    e4: cc.Node = null;

    @property(cc.Node)
    e5: cc.Node = null;

    @property(cc.Node)
    e6: cc.Node = null;

    @property(cc.Node)
    e7: cc.Node = null;

    @property(cc.Node)
    e8: cc.Node = null;

    @property(cc.Node)
    e9: cc.Node = null;

    @property(cc.Node)
    e10: cc.Node = null;

    @property(cc.Node)
    e11: cc.Node = null;

    @property(cc.Node)
    e12: cc.Node = null;

    @property(cc.Node)
    e13: cc.Node = null;

    @property(cc.Node)
    e14: cc.Node = null;

    @property(cc.Node)
    e15: cc.Node = null;

    @property(cc.Node)
    e16: cc.Node = null;

    @property(cc.Node)
    e17: cc.Node = null;

    @property(cc.Node)
    e18: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //start point
        this.p1.position = cc.v2(-this.node.parent.width/2*1.4, 158);
        this.p2.position = cc.v2(-this.node.parent.width/2*0.58, 325);
        this.p3.position = cc.v2(0, 85);
        this.p4.position = cc.v2(this.node.parent.width/2*0.58, 196);

        this.p5.position = cc.v2(this.node.parent.width/2*1.4, 158);
        this.p6.position = cc.v2(this.node.parent.width/2*0.58, 325);
        this.p7.position = cc.v2(0, 85);
        this.p8.position = cc.v2(-this.node.parent.width/2*0.58, 196);

        //end points
        var x1 = -this.node.parent.width/2 + 1*this.node.parent.width/7;
        var x2 = -this.node.parent.width/2 + 2*this.node.parent.width/7;
        var x3 = -this.node.parent.width/2 + 3*this.node.parent.width/7;
        var x4 = -this.node.parent.width/2 + 4*this.node.parent.width/7;
        var x5 = -this.node.parent.width/2 + 5*this.node.parent.width/7;
        var x6 = -this.node.parent.width/2 + 6*this.node.parent.width/7;

        this.e1.position = cc.v2(x1, 328);
        this.e2.position = cc.v2(x2, 328);
        this.e3.position = cc.v2(x3, 328);
        this.e4.position = cc.v2(x4, 328);
        this.e5.position = cc.v2(x5, 328);
        this.e6.position = cc.v2(x6, 328);

        this.e7.position = cc.v2(x1, 265);
        this.e8.position = cc.v2(x2, 265);
        this.e9.position = cc.v2(x3, 265);
        this.e10.position = cc.v2(x4, 265);
        this.e11.position = cc.v2(x5, 265);
        this.e12.position = cc.v2(x6, 265);

        this.e13.position = cc.v2(x1, 200);
        this.e14.position = cc.v2(x2, 200);
        this.e15.position = cc.v2(x3, 200);
        this.e16.position = cc.v2(x4, 200);
        this.e17.position = cc.v2(x5, 200);
        this.e18.position = cc.v2(x6, 200);
        
        
    }
    
    start () {

    }

    // update (dt) {}
}
