import { Helper } from "./Helper";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SequencePoints extends cc.Component {

    @property([cc.Vec2])
    public points: cc.Vec2[] = [];

    onLoad(){
        if(this.points.length == 0) {
            for(var i=0; i<this.node.children.length; i++) {
                this.points.push(this.node.children[i].getPosition());
                // console.log("position" + this.node.children[0].getPosition().toString());
            }    
        }
        
    }

}