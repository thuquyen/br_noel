import HPBar from "./HpBar";
import G from "./Global";
import ShipController from "./ShipController";
import WaveController from "./WaveController_Old";
import CanvasController from "./CanvasController";
import HandController from "./HandController";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

@ccclass
export default class GameController_Old extends cc.Component {

    @property(CanvasController)
    canvas: CanvasController = null;
    @property(ShipController)
    ship: ShipController = null;
    @property(WaveController)
    wave: WaveController = null;
    @property(HandController)
    hand: HandController = null;
    collisionManager: cc.CollisionManager = null;

    gameState = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.gameState = GameState.INIT;
        this.canvas.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.canvas.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.canvas.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.canvas.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        this.collisionManager = cc.director.getCollisionManager();
        this.collisionManager.enabled = true;

        this.ship.onRevive = ()=>{
            this.hand.runSwipeAnim(this.ship.node.x, this.ship.node.y - 30);
        };

        this.ship.onDie = ()=>{
            this.hand.stopSwipeAnim();
            this.scheduleOnce(()=>{
                this.ship.revive();
            }, 1);
        };
    }

    start () {
        this.startAppearAnim();
    }

    onTouchStart(event: cc.Touch){
        switch(this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch(this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch(this.gameState) {
            case GameState.PLAYING:
                if(!this.ship.isDie) {
                    this.hand.runSwipeAnim(this.ship.node.x, this.ship.node.y - 30);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch(this.gameState) {
            case GameState.PLAYING:
                break;
        }
    }

    startGame(){
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.canvas.hideStoryText();
        this.ship.setEnableMove(true);
        this.ship.setEnableBullet(true);
        this.wave.startAttack();
        this.wave.setOnFinishWave(this.onEndGame.bind(this));
    }

    startAppearAnim(){
        this.wave.appear(()=>{
        });
        this.canvas.appear(()=>{
            this.hand.runSwipeAnim(this.ship.node.x, this.ship.node.y - 30);
            this.gameState = GameState.READY;
        });
    }

    private onEndGame(){
        this.gameState = GameState.ENDGAME;
        this.ship.setEnableProtect(true);
        this.hand.stopSwipeAnim();
        this.scheduleOnce(()=>{
            this.canvas.hideInstallToPlay();
            this.ship.flyOut(()=>{
                this.canvas.showWinPopup();
            });
        }, 1);
    }

    //update (dt) {}
}