import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundManager extends cc.Component {
    // @property({
    //     type: cc.AudioClip
    // })
    // bg: cc.AudioClip = null;

    // @property(cc.AudioClip)
    // bossExplosion: cc.AudioClip = null;

    @property(cc.AudioClip)
    enemyDie: cc.AudioClip = null;
    @property({type: cc.AudioClip })
    shooting: cc.AudioClip = null;

    @property({ type: cc.AudioClip})
    powerUp: cc.AudioClip = null;

    @property({type: cc.AudioClip})
    flyDie: cc.AudioClip = null;

 
    // @property(cc.AudioClip)
    // playerExplosion: cc.AudioClip = null;
    // @property(cc.AudioClip)
    // revive: cc.AudioClip = null;
    // @property(cc.AudioClip)
    // warning: cc.AudioClip = null;

    onLoad() {
        //cc.loader.loadRes('Audio', this.bg);
        //cc.audioEngine.playMusic(this.bg, true);

        G.enemyDie = this.enemyDie;
        G.hitBullet = this.shooting;
        G.flyDie = this.flyDie;
        G.powerUp = this.powerUp;
    }
}
