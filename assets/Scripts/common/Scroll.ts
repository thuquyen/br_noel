

const {ccclass, property} = cc._decorator;

@ccclass
export default class Scroll extends cc.Component {

    @property()
    private speed = 0;

    @property()
    private resetY = 1351;

    private y0;

    start() {
        this.y0 = this.node.y;
    }

    update (dt) {
        var y = this.node.y;
        y += this.speed * dt;
        if(y<= this.resetY) {
            y = this.y0;
        }
        this.node.y = y;
    }
}