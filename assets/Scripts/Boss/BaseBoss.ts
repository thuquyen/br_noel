import HPBar from "../HpBar";
import BossState from "../BossState";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseBoss extends cc.Component {
    @property(HPBar)
    protected hpBar: HPBar = null;

    @property()
    HPMAX = 32;

    private hp = 0;

    @property({type: cc.Enum(BossState)})
    protected bossState:BossState = BossState.INIT;

    @property(cc.Prefab)
    protected bulletExplosion: cc.Prefab = null;

    @property(cc.Prefab)
    protected bossExplosionPrefab: cc.Prefab = null;

    public onDie: Function;

    onLoad(){
        this.hp = this.HPMAX;
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if(other.node.group == 'shipBullet') {
            this.onHitShipBullet(other.node);
        }
        else if(other.node.group == 'ship') {
            
        }
    }

    private onHitShipBullet(node: cc.Node){
        var pos = this.node.convertToNodeSpaceAR(node.parent.convertToWorldSpaceAR(node.getPosition()));
        this.runBulletExplosionEffect(this.node, pos.x, pos.y, 1.2/this.node.scaleX, 2);
        node.destroy();
        if(this.bossState == BossState.ATTACK) {
            this.hp -= 1;
            if(this.hpBar)
                this.hpBar.setPercent(this.hp/this.HPMAX);
            if(this.hp <= 0) {
                this.onBossDie();
            }
        }
    }

    private runBulletExplosionEffect(parent: cc.Node, x: number, y: number, scale: number, speed: number){
        var explosion = cc.instantiate(this.bulletExplosion);
        explosion.parent = parent;
        explosion.x = x;
        explosion.y = y;
        explosion.scale = scale;
        var clip = explosion.getComponent(cc.Animation).defaultClip;
        clip.speed = speed;
        var t = clip.duration/clip.speed;
        this.scheduleOnce(()=>{
            explosion.destroy();
        }, t);
    }

    runBossExplosionEffect(parent: cc.Node, x: number, y: number, scale: number, speed: number){
        var explosion = cc.instantiate(this.bossExplosionPrefab);
        explosion.parent = parent;
        explosion.x = x;
        explosion.y = y;
        explosion.scale = scale;
        var clip = explosion.getComponent(cc.Animation).defaultClip;
        clip.speed = speed;
        var t = clip.duration/clip.speed;
        explosion.getComponent(cc.Animation).scheduleOnce(()=>{
            explosion.destroy();
        }, t);
    }

    @property(cc.Vec2)
    adjustExplosion: cc.Vec2 = new cc.Vec2(0, 100);

    private onBossDie(){
        this.bossState = BossState.DIE;
        this.runBossExplosionEffect(this.node.parent, this.node.x + this.adjustExplosion.x, this.node.y + this.adjustExplosion.y, 5, 1.5);
        this.node.destroy();
        if(this.onDie) {
            this.onDie(this);
        }
    }

    public appear(onDone: Function){
        this.node.y += 300;
        this.node.runAction(
            cc.sequence(
                cc.moveBy(0.8, 0, -300).easing(cc.easeBackOut()),
                cc.callFunc(()=>{
                    if(onDone)
                        onDone();
                })
            )
        );
    }

    public startAttack(){
        this.bossState = BossState.ATTACK;
        this.hp = this.HPMAX;
    }

}