import Global from "../Global";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletEnemy extends cc.Component {

    @property(cc.Prefab)
    public bulletEnemy: cc.Prefab[] = [];

    @property()
    private timeBullet: number = 0;

    @property()
    private interval: number = 1.5;

    @property()
    private intervalRandom: number = 0;

    @property()
    private delay: number = 0;

    @property()
    private delayRandom: number = 0;

    @property(sp.Skeleton)
    animBoss: sp.Skeleton = null;
    str: string = '';
    typeBullet: number = 1;
    // onLoad () {}

    start() {
        this.animBoss = this.getComponent(sp.Skeleton);
        this.animBoss.setMix("idle", "attack_l_hand", 0.15);
        this.animBoss.setMix("attack_l_hand", "idle", 0.15);
        this.animBoss.setMix("idle", "attack_r_hand", 0.15);
        this.animBoss.setMix("attack_r_hand", "idle", 0.15);
        this.changAnim(0, "idle", true);

        this.schedule(() => {
            if (Global.enableBulletBoss) {
                Global.typeBulletBoss = 0;
                this.changAnim(1, "attack_l_hand", true);
                this.scheduleOnce(() => {
                    this.changAnim(1, "idle", true);
                }, 1.3)
                this.scheduleOnce(() => {
                    this.spawnBulletEnemy();
                }, 1.45)

                this.scheduleOnce(() => {
                    if (Global.enableBulletBoss) {
                        Global.typeBulletBoss = 1;
                        this.changAnim(1, "attack_r_hand", true);
                        this.scheduleOnce(() => {
                            this.changAnim(1, "idle", true);
                        }, 1.3)
                        this.scheduleOnce(() => {
                            this.spawnBulletEnemy();
                        }, 1.4)
                    }
                }, 1.5)
            }
        }, 3)

        // this.startBulletEnemy();
    }

    changAnim(track: number, animState: string, bool: boolean) {

        if (animState === this.str) {
            return;
        }
        this.animBoss.setAnimation(track, animState, bool);
        this.str = animState;
    }

    //BULLET ENEMY
    private spawnBulletEnemy() {
        if (this.node.activeInHierarchy) {
            if (Global.enableBulletBoss) {
                var newBulletEnemy = cc.instantiate(this.bulletEnemy[Global.typeBulletBoss]);
                newBulletEnemy.parent = cc.Canvas.instance.node;
                newBulletEnemy.x = this.node.x;
                newBulletEnemy.y = this.node.y - 5;
            }
        } else {
            return;
        }
    }
    private startBulletEnemy() {
        if (G.enableBulletBoss) {
            this.schedule(this.spawnBulletEnemy, this.interval, cc.macro.REPEAT_FOREVER, this.delay);
        }
    }
}
