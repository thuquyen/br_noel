const { ccclass, property } = cc._decorator;

@ccclass
export default class skillDelay extends cc.Component {
    @property(cc.Node)
    private fire: cc.Node = null;
    @property(cc.Prefab)
    bulletPrefab: cc.Prefab = null;
    @property()
    public timeFire: number = 0;
    onLoad() {
        this.getFire();
    }
    getFire() {
        this.fire.runAction(cc.sequence(cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(this.fire.angle * Math.PI / 180), -2000)), cc.callFunc(() => {
            this.fire.destroy();
        })));
        this.scheduleOnce(() => {
            for (let i = 0; i < 7; i++) {
                var bullet = cc.instantiate(this.bulletPrefab);
                bullet.parent = cc.Canvas.instance.node;
                bullet.x = this.node.x + (Math.random() * 220);
                bullet.y = this.node.y + 110 * (Math.random() * 2 - 1);
                bullet.runAction(cc.sequence(cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(bullet.angle * Math.PI / 180), -2000)), cc.callFunc(() => {
                    bullet.destroy();
                })));
            }
        }, 0.08)
    }
}