import Global from "../Global";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SkillBossNoel extends cc.Component {

    @property(cc.Prefab)
    public bulletEnemy: cc.Prefab[] = [];

    @property()
    private timeBullet: number = 0;

    @property()
    private interval: number = 2.5;

    @property()
    private intervalRandom: number = 0;

    @property()


    private delay: number = 0;

    @property()
    private delayRandom: number = 0;

    @property(sp.Skeleton)
    animBoss: sp.Skeleton = null;
    str: string = '';
    typeBullet: number = 1;
    timeInterval = 10;
    isRun: boolean = false;

    onLoad() {
        Global.typeBulletBoss = 3;
    }

    start() {
        this.animBoss = this.getComponent(sp.Skeleton);
        this.animBoss.setMix("idle", "skill3", 0.15);
        this.animBoss.setMix("skill3", "idle", 0.15);
        this.animBoss.setMix("idle", "skill5", 0.15);
        this.animBoss.setMix("skill5", "idle", 0.15);
        this.changAnim(0, "idle", true);

        //Skill3: circle snow from two award
        //Skill 5: ice bullet from high
        // Idle 
        this.startBulletEnemy();
    }
    boosShoot() {
        this.schedule(() => {
            if (Global.enableBulletBoss) {
                Global.typeBulletBoss = 0;
                this.changAnim(1, "skill3", true);
                this.scheduleOnce(() => {
                    Global.typeBulletBoss = 3;
                    this.changAnim(1, "idle", true);
                    this.scheduleOnce(() => {
                        Global.typeBulletBoss = 2;
                        this.changAnim(1, "skill5", true);
                        this.scheduleOnce(() => {
                            Global.typeBulletBoss = 3;
                            this.changAnim(1, 'idle', true);
                        }, 3.2)
                    }, 1.5)
                }, 4.2)
            }
            else {
                this.changAnim(1, "idle", true);
            }
        }, this.timeInterval, cc.macro.REPEAT_FOREVER, 1)
    }

    changAnim(track: number, animState: string, bool: boolean) {
        if (animState === this.str) {
            return;
        }
        this.animBoss.setAnimation(track, animState, bool);
        this.str = animState;
    }

    //BULLET ENEMY
    private spawnBulletEnemy() {
        if (this.node.activeInHierarchy) {
            if (Global.enableBulletBoss) {
                if (Global.typeBulletBoss == 0) {
                    var newBulletEnemy = cc.instantiate(this.bulletEnemy[1]);
                    newBulletEnemy.parent = cc.Canvas.instance.node;
                    newBulletEnemy.x = this.node.x;
                    newBulletEnemy.y = this.node.y - 5;
                    this.scheduleOnce(() => {
                        var newBulletEnemy = cc.instantiate(this.bulletEnemy[0]);
                        newBulletEnemy.parent = cc.Canvas.instance.node;
                        newBulletEnemy.x = this.node.x;
                        newBulletEnemy.y = this.node.y - 5;
                    }, 0.5)
                }
                else if (Global.typeBulletBoss == 2) {
                    var newBulletEnemy = cc.instantiate(this.bulletEnemy[2]);
                    newBulletEnemy.parent = cc.Canvas.instance.node;
                    newBulletEnemy.x = this.node.x;
                    newBulletEnemy.y = this.node.y - 5;
                }
            }
        } else {
            return;
        }
    }
    private startBulletEnemy() {
        this.schedule(this.spawnBulletEnemy, this.interval, cc.macro.REPEAT_FOREVER, this.delay);
    }
    update(dt) {
        if (G.shootBoss && !this.isRun) {
            this.boosShoot();
            this.isRun = true;
            G.shootBoss = false;
        }
    }
}
