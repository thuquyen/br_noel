import { Helper } from "./Helper";
import BaseEnemy from "./Enemy/BaseEnemy";

const {ccclass, property} = cc._decorator;

@ccclass('BlockInfo')
export default class BlockInfo {

    @property(cc.Node)
    public nodeStartPath: cc.Node = null;

    @property(cc.Node)
    public nodeEndPoints: cc.Node = null;

    public startPath: cc.Vec2[] = [];

    public endPoints: cc.Vec2[] = [];

    @property(cc.Prefab)
    public prefab: cc.Prefab = null;

    @property([cc.Prefab])
    public prefabs: cc.Prefab[] = [];

    @property(Number)
    public delay: number = 0;
    @property(Number)
    public timeDistance: number = 0.2;
    @property(Number)
    public speed: number = 200;
    @property(Number)
    public angleOffset: number = 90;
    @property(Boolean)
    public isRotate: boolean = true;

    public enemies: BaseEnemy[] = null;

    public onComplete: Function = null;

    private _nEnemy: number = 0;

    constructor(){
        this.enemies = [];
    }

    appear(parent: cc.Node, componentSchedule: cc.Component, onDone: Function){
        this._nEnemy = 0;
        if(((this.startPath === null) || (this.startPath.length == 0)) && (this.nodeStartPath != null)) {
            this.startPath = Helper.getSequencePoints(this.nodeStartPath);
        }
        if(((this.endPoints === null) || (this.endPoints.length == 0)) && (this.endPoints != null)) {
            this.endPoints = Helper.getSequencePoints(this.nodeEndPoints);
        }
        this.endPoints.forEach((endPoint, i)=>{
            if(this.isRotate) {
                componentSchedule.scheduleOnce(()=>{
                    var e = this.spawnEnemy(i);
                    e.angle = parent.angle;
                    e.x = 1000;
                    e.y = 1000;
                    e.parent = parent;
                    this._nEnemy++;
                    e.getComponent(BaseEnemy).onDie = (()=>{
                        this._nEnemy--;
                        if(this._nEnemy<=0) {
                            if(this.onComplete)
                                this.onComplete();
                        }
                    });
                    this.enemies.push(e.getComponent(BaseEnemy));
                    Helper.splineFollowAndRotate(e, this.startPath, this.speed, 0, this.angleOffset, ()=>{
                        Helper.moveToAndAdjustAngle(e, endPoint, this.speed/1.5, ()=>{
                            if(i == this.endPoints.length-1) {
                                //this.setupCompleteCallback();
                                if(onDone)
                                    onDone();
                            }
                        });
                    });
                    }, i*this.timeDistance);
            } else {
                componentSchedule.scheduleOnce(()=>{
                    var e = this.spawnEnemy(i);
                    e.angle = parent.angle;
                    e.x = 1000;
                    e.y = 1000;
                    e.parent = parent;
                    this._nEnemy++;
                    e.getComponent(BaseEnemy).onDie = (()=>{
                        this._nEnemy--;
                        if(this._nEnemy<=0) {
                            if(this.onComplete)
                                this.onComplete();
                        }
                    });
                    this.enemies.push(e.getComponent(BaseEnemy));
                    Helper.splineFollow(e, this.startPath, this.speed, 0, this.angleOffset, ()=>{
                        Helper.moveTo(e, endPoint, this.speed/1.5, ()=>{
                            if(i == this.endPoints.length-1) {
                                //this.setupCompleteCallback();
                                if(onDone)
                                    onDone();
                            }
                        });
                    });
                }, i*this.timeDistance);
            }
        });
    }

    private spawnEnemy(i){
        if(this.prefabs && this.prefabs.length > 0 && this.prefabs.length>i) {
            return cc.instantiate(this.prefabs[i]);
        } else {
            return cc.instantiate(this.prefab);
        }
    }

}