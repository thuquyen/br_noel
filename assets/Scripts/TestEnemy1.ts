import BaseEnemy from "./Enemy/BaseEnemy";
import SequencePoints from "./SequencePoints";
import { Helper } from "./Helper";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class TestEnemy1 extends BaseEnemy {

    @property(SequencePoints)
    splinePath: SequencePoints = null;

    @property()
    delay: number = 0;

    // @property()
    // duration: number = 3;

    @property(Number)
    speed: number = 500;

    @property()
    angleOffset: number = 90;

    start(){
        super.start();
        Helper.splineFollowAndRotate(this.node, this.splinePath.points, this.speed, this.delay, this.angleOffset, null);
    }

}