import G from "./Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        
    }

    update (dt) {
        if(G.e === true){
            this.pauseMoveAround();
            this.getComponent(sp.Skeleton).timeScale = 0;
        }else{
            this.resumeMoveAround();
            this.getComponent(sp.Skeleton).timeScale = 0.8;
        }
    }

    pauseMoveAround(){
        this.getComponent(cc.Animation).pause();
    }

    resumeMoveAround(){
        this.getComponent(cc.Animation).resume();
    }

    
}
