import BaseEnemy from "./Enemy/BaseEnemy";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseEnemyInfo extends cc.Component {

    @property(cc.Prefab)
    prefabEnemy: cc.Prefab = null;

    @property(Number)
    hp: number = 2;

    @property(Number)
    dmg: number = 0;

    spawnEnemy(): cc.Node {
        var enemy = cc.instantiate(this.prefabEnemy);
        var baseEnemy = enemy.getComponent(BaseEnemy);
        enemy.parent = this.node;
        baseEnemy.hp = this.hp;
        baseEnemy.dmg = this.dmg;
        return enemy;
    }

}