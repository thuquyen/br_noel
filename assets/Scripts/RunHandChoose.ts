

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    shipChoose: cc.Node[] = [];
    @property()
    timeActive: number = 0;
    @property(cc.Node)
    handNode: cc.Node = null;
    @property()
    numberShip: number = 1;
    // LIFE-CYCLE CALLBACKS:
    @property(cc.Node)
    hand: cc.Node = null;
    @property(cc.Node)
    handIdel: cc.Node = null;
    // private temp: number = 35;
    // // onLoad () {}
    // //spine ship different for pivot and scale so +temp
    // start() {
    //     this.schedule(() => {
    //         if (this.handNode.active && this.shipChoose[0].active && this.shipChoose[1].active) {
    //             if (this.numberShip == 0) {
    //                 this.temp = 35;
    //             }
    //             else {
    //                 this.temp = 0;
    //             }
    //             this.handNode.runAction(cc.sequence(cc.moveTo(1.1, this.shipChoose[this.numberShip].getPosition().x + 10, this.shipChoose[this.numberShip].getPosition().y + this.temp),
    //                 cc.callFunc(() => {
    //                     this.handIdel.opacity = 0;
    //                     this.hand.opacity = 255;
    //                     this.scheduleOnce(() => {
    //                         this.hand.opacity = 0;
    //                         this.handIdel.opacity = 255
    //                     }, 1.6);
    //                 })
    //             ));

    //             this.numberShip++;
    //             if (this.numberShip > 1) {
    //                 this.numberShip = 0;
    //             }
    //         }
    //     }, 3, cc.macro.REPEAT_FOREVER, this.timeActive);
    // }
    start() {
        this.schedule(() => {
            if (this.shipChoose[0].isValid && this.shipChoose[1].isValid) {
                this.handNode.runAction(cc.sequence(cc.moveTo(1.1, this.shipChoose[this.numberShip].getPosition().x + 10, this.shipChoose[this.numberShip].getPosition().y),
                    cc.callFunc(() => {
                        this.handIdel.opacity = 0;
                        this.hand.opacity = 255;
                        this.scheduleOnce(() => {
                            this.hand.opacity = 0;
                            this.handIdel.opacity = 255
                        }, 1.6);
                    })));

                this.numberShip++;
                if (this.numberShip > 1) {
                    this.numberShip = 0;
                }
            }
        }, 3, cc.macro.REPEAT_FOREVER, this.timeActive);
    }

    // update (dt) {}
}
