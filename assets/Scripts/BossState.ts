enum BossState {
    INIT = 0,
    ATTACK = 1,
    DIE = 2
};

export default BossState;