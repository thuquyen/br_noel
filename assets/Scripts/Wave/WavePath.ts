import PathInfo from "../PathInfo";
import BlockInfo from "../BlockInfo";
import IWave from "./BaseWave";
import GroupPathEnemy from "../Enemy/GroupPathEnemy";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class WavePath extends IWave {

    @property([PathInfo])
    pathInfo: PathInfo[] = [];

    @property([BlockInfo])
    blockInfo: BlockInfo[] = [];

    startWave() {
        var nComplete = this.pathInfo.length + this.blockInfo.length;
        var onPartialComplete = ()=>{
            nComplete--;
            if(nComplete <= 0)
                if(this.onComplete)
                    this.onComplete();
        };
        for(var i=0; i<this.pathInfo.length; i++) {
            var p = this.pathInfo[i];
            p.appear(this.node, null);
            p.onComplete = onPartialComplete;
        }
        for(var i=0; i<this.blockInfo.length; i++) {
            this.blockInfo[i].appear(this.node, this, null);
            this.blockInfo[i].onComplete = onPartialComplete;
        }
    }

}