import PathInfo from "../PathInfo";
import BlockInfo from "../BlockInfo";
import IWave from "./IWave";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WaveQuangCao extends IWave {

    @property([PathInfo])
    pathInfo: PathInfo[] = [];

    @property([BlockInfo])
    blockInfo: BlockInfo[] = [];
    private timeDelay: number = 0;

    appear(onDone: Function) {
        var total = this.pathInfo.length + this.blockInfo.length;
        var checkDone = () => {
            total--;
            if (total <= 0)
                if (onDone)
                    onDone();
        }
        var nComplete = this.pathInfo.length + this.blockInfo.length;
        var onPartialComplete = () => {
            nComplete--;
            if (nComplete <= 0)
                if (this.onComplete)
                    this.onComplete();
        };
        for (var i = 0; i < this.pathInfo.length; i++) {
            var p = this.pathInfo[i];
            p.appear(this.node, checkDone);
            p.onComplete = onPartialComplete;
        }
        for (var i = 0; i < this.blockInfo.length; i++) {
            this.blockInfo[i].appear(this.node, this, checkDone);
            this.blockInfo[i].onComplete = onPartialComplete;
        }
    }

    startWave() {
    }

}