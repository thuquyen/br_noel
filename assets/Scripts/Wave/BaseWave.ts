import IWave from "./IWave";
import IEnemy from "../Enemy/IEnemy";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseWave extends IWave {

    @property([IEnemy])
    public enemies: IEnemy[] = [];

    startWave () {
        let n = this.enemies.length;
        for(let i=0; i<this.enemies.length; i++) {
            this.enemies[i].onDie = ()=>{
                n--;
                if(n==0) {
                    if(this.onComplete)
                        this.onComplete();
                }
            }
        }
    }

}